<?php
//<editor-fold desc="Head INIT">
$siteVars = [];
// Cargar funciones generales y variables del sitio
require __DIR__.'/snkeng/core/php/load/head.php';

$siteMap = <<<XML
<?xml version='1.0' encoding='UTF-8'?>
<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">\n
XML;
$urlStr = <<<XML
<url><loc>{$siteVars['server']['url']}!url;</loc><lastmod>!dtMod;</lastmod></url>\n
XML;

$sql_qry = <<<SQL
SELECT
art_url AS url,
DATE(GREATEST(art_dtmod, art_dtpub)) AS dtMod
FROM st_site_articles
WHERE art_published=1 AND art_indexable=1 AND art_dtpub<now();
SQL;
$siteMap.= $mysql->printSimpleQuery($sql_qry, $urlStr);


$apps = [];
foreach ( $apps as $app ) {
	$file = SNKENG_DIR."/st_content/apps/{$app}/sitemap.php";
	if ( file_exists($file) ) {
		$file = (require($file));
		$siteMap.= $mysql->printSimpleQuery($file['sql'], $urlStr);
	} else {
		if ( $siteVars['sitez']['test'] ) {
			die("Archivo no válido.\nApp: {$app}.\nFile: {$file}\n");
		}
	}
}

// Fin
$siteMap.= "</urlset>";

// Enviar información
// $siteVars['server']['test'] = true;
if ( strstr($_SERVER['HTTP_USER_AGENT'], 'W3C_Validator') !== false || strstr($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip') === false || $siteVars['server']['test'] ){
	// do not compress
	header('Content-type: text/xml; charset=UTF-8');
}else{
	// can send GZIP compressed data
	header('content-type: application/x-gzip');
	header('Content-Disposition: attachment; filename="sitemap.gz"');
	$siteMap = gzencode($siteMap, 9);
}
// Imprimir contenido
echo $siteMap;
exit();