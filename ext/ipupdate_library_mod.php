<?php
//
header('Content-type: text/plain; charset=utf-8');

// Definitions
DEFINE('CURL_TIMEOUT', 15);
DEFINE('IPV4_REGEX', '/(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})/');
DEFINE('IPV6_REGEX', '/(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))/');
DEFINE('CHECK_IPV6', "http://checkipv6.dyndns.org:8245");
DEFINE('CHECK_IP', "http://checkip.dyndns.org:8245");
DEFINE('API_URL', "https://api.digitalocean.com/v2/");

//
function debugVariable($var, $name='Not Defined', $kill=true)
{
    //
    //
    $pVar = '';
    $pType = gettype($var);

    switch ( $pType ) {
        case 'boolean':
        case 'integer':
        case 'double':
        case 'string':
            $pVar = $var;
            break;
        case 'array':
        case 'object':
        case 'resource':
            $pVar = print_r($var, true);
            break;
        case 'null':
            break;
        default:
            $pType = 'Desconocido';
            $pVar = $var."\n\n---\n\n".print_r($var, true);
            break;
    }
    //
    echo <<<TEXT
\n
<pre>
--- DEBUGVARIABLE ---
Name: {$name}.
Type: {$pType}.
Var:
--- INI ---
{$pVar}
--- END ---
---
</pre>\n\n
TEXT;

    // Terminar operación?
    if ( $kill ) { exit(); }
}
//
function debugUpdate($string) {
    echo $string."\n";
}

//
function get_client_ip() {

	$result = null;

	$ipSourceList = [
		'HTTP_CLIENT_IP','HTTP_X_FORWARDED_FOR',
		'HTTP_X_FORWARDED', 'HTTP_FORWARDED_FOR',
		'HTTP_FORWARDED', 'REMOTE_ADDR'
	];

	//
	foreach ( $ipSourceList as $ipSource ) {
		if ( getenv($ipSource) ) {
			$tempIp = getenv($ipSource);
			echo "$tempIp\n";
			if ( $tempIp !== '127.0.0.1' ) {
				$result = $tempIp;
				break;
			}
		}
	}

	//
	if ( !$result ) {
		die("No fue posible obtener el ip.");
	}

	return $result;
}

//
function get_client_ip_2() {

	debugVariable($_SERVER);


	if ( getenv('HTTP_CLIENT_IP') && getenv('HTTP_CLIENT_IP') !== '127.0.0.1' ) {
		$ipaddress = getenv('HTTP_CLIENT_IP');
	}
	else if(getenv('HTTP_X_FORWARDED_FOR') && getenv('HTTP_X_FORWARDED_FOR') !== '127.0.0.1' ) {
		$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
	}
	else if(getenv('HTTP_X_FORWARDED') && getenv('HTTP_X_FORWARDED') !== '127.0.0.1' ) {
		$ipaddress = getenv('HTTP_X_FORWARDED');
	}
	else if(getenv('HTTP_FORWARDED_FOR') && getenv('HTTP_FORWARDED_FOR') !== '127.0.0.1' ) {
		$ipaddress = getenv('HTTP_FORWARDED_FOR');
	}
	else if(getenv('HTTP_FORWARDED') && getenv('HTTP_FORWARDED') !== '127.0.0.1' ) {
		$ipaddress = getenv('HTTP_FORWARDED');
	}
	else if(getenv('REMOTE_ADDR') && getenv('REMOTE_ADDR') !== '127.0.0.1' ) {
		$ipaddress = getenv('REMOTE_ADDR');
	}
	else {
		$ipaddress = 'UNKNOWN';
		die("Invalid address reported");
	}
	return $ipaddress;
}

/**
 * Return the contents of a page (DO only uses GET requests for its API...)
 *
 * @param $url string
 *
 * @return mixed
 */
function getWebPage($url)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, CURL_TIMEOUT);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    $data = curl_exec($ch);
    curl_close($ch);
    return $data;
}
/**
 * Retrieve our external IP address
 *
 * @return mixed
 */
function getExternalIp($method)
{
	switch ( $method ) {
		case 'ipv4':
			$site = CHECK_IP;
			$regex = IPV4_REGEX;
			break;
		case 'ipv6':
			$site = CHECK_IPV6;
			$regex = IPV6_REGEX;
			break;
		default:
			echo "Undefined protocol for external ip '{$method}'.";
			return false;
			break;
	}

	// Get ipv4
	$html = getWebPage($site);
	if ( preg_match($regex, $html, $matches) === 0 ) {
		return false;
	}
	return $matches[0];
}
/**
 * Given the domain ID, fetch all Records and return the one we want.
 *
 * @param null $page
 * @return bool
 */
function getAllRecords($domain, &$results, $page = null)
{
    // Parameters
    $options = [
        CURLOPT_URL => API_URL . 'domains/' . $domain . '/records' . ( ( $page !== null ) ? '?page=' . $page : ''),
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CONNECTTIMEOUT => CURL_TIMEOUT,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_SSL_VERIFYHOST => 2,
        CURLOPT_HTTPHEADER => [
            'Authorization: Bearer ' . ACCESS_TOKEN
        ]
    ];

    // Request
    $curlHandle = curl_init();
    curl_setopt_array($curlHandle, $options);
    $data = curl_exec($curlHandle);
    $info = curl_getinfo($curlHandle);
    curl_close($curlHandle);

    $responseCode = $info['http_code'];
    if ( $data === false || $responseCode !== 200 ) {
        return false;
    }

    $dataJson = json_decode($data, true);

	foreach ( $dataJson['domain_records'] as $record ) {
		$results[] = $record;
	}
	// Recursive call for pages results
	if ( isset($dataJson['links']['pages']['next']) && $dataJson['links']['pages']['next'] != '' ) {
		preg_match('/page=(?<page_number>\d+)/i', $dataJson['links']['pages']['next'], $match);
		if ( isset($match['page_number']) && $match['page_number'] !== '' ) {
			return getAllRecords($domain, $results, $match['page_number']);
		}
	}
	//
	return true;
}

/**
 * Update the DO Domain IP address, providing the IPAddress, the Domain and Record ID.
 *
 * @param $record
 * @param $ipAddress
 *
 * @return mixed|string
 * @throws Exception
 */
function setRecordIP($domain, $recordId, $ipAddress)
{
	// Data
	$data = json_encode(['data' => $ipAddress]);

	// Configuration
	$options = [
		CURLOPT_URL => API_URL . 'domains/' . $domain . '/records/' . $recordId,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_CUSTOMREQUEST => "PUT",
		CURLOPT_POSTFIELDS => $data,
		CURLOPT_CONNECTTIMEOUT => CURL_TIMEOUT,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_SSL_VERIFYHOST => 2,
		CURLOPT_HTTPHEADER => [
			'Authorization: Bearer ' . ACCESS_TOKEN,
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data)
		]
	];

	// Request
	$curlHandle = curl_init();
	curl_setopt_array($curlHandle, $options);
	$data = curl_exec($curlHandle);
	curl_close($curlHandle);

    return ($data !== false);
}


//
function updateRecord($accessToken, $realData, $method) {
    //
    try {
        if ( !isset($accessToken) ) {
            throw new Exception('1st parameter (Access Token) is missing.');
        }

        DEFINE('ACCESS_TOKEN', $accessToken);        //Digital Ocean Personal Access Tokens (read & write)

	    //
	    $updates = [];
	    $ipAddress = "";
	    $recordType = ( $method === 'ipv4' ) ? 'A' : 'AAAA';

	    // Get IP
	    debugUpdate('Fetching external IP from: ' . CHECK_IP);
	    // if ( ( $ipAddress = getExternalIp($method) ) === false ) {
	    if ( ( $ipAddress = get_client_ip() ) === false ) {
		    throw new Exception('Unable to extract external IP address');
	    }
	    debugUpdate('Current location IP: ' . $ipAddress."\n\n");

        //
        foreach ( $realData as $domain => $params ) {
        	$records = [];
	        // Get all available data
	        debugUpdate("Dominio: {$domain}.");
	        getAllRecords($domain, $records);
	        if ( empty( $records ) ) {
		        throw new Exception('No records in this domain... (it\'s an error).');
	        }

	        //
	        foreach ( $params as $param ) {
	        	debugUpdate("Registro: {$param}.");
	        	$found = false;
	            foreach ( $records as $record ) {
	            	if ( $record['type'] === $recordType && $record['name'] === $param ) {
			            if ( $record['data'] === $ipAddress ) {
				            debugUpdate("- Mismo IP, ignorar.");
			            } else {
				            debugUpdate("- Actualizar: ({$record['data']})");
				            $updates[] = $record['id'];
			            }
	            		$found = true;
	            		break;
		            }
	            }
	            //
	            if ( !$found ) {
		            throw new Exception("ERROR: no se encontró en el sistema.");
	            }
	        }

	        // debugVariable($records, 'records', false);
	        // debugVariable($updates, 'records', false);


	        // Iniciar querys de actualización
	        debugUpdate("Iniciando actualizaciones:");
	        foreach ( $updates as $cUpdate ) {
		        debugUpdate("- {$cUpdate}");
		        if ( setRecordIP($domain, $cUpdate, $ipAddress) === false ) {
			        throw new Exception('Unable to update IP address');
		        }
	        }
	        debugUpdate("Fin dominio\n\n");
        }


    } catch (Exception $e) {
        echo 'Error: ' . $e->getMessage() . "\r\n";
        exit(1);
    }
    exit(0);
}
