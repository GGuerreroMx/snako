<?php
// PW

// =====================================
//	FUNCIONES
// =====================================
// {

function cleanData($string)
{
	$string = strtolower(trim(preg_replace("/\s+/", " ", $string)));
	return $string;
}
function md5_base64($data)
{
    return preg_replace('/=+$/', '', base64_encode(pack('H*',md5($data))));
}
function sha1_base64_cleaned($data) {
	return preg_replace('/[^A-Za-z0-9]/', '', base64_encode(sha1($data, true)));
}
function newHash($data) {
	echo("INI: ".$data."<br />");
	$data1 = md5($data);
	echo("MD5: ".$data1."<br />");
	$data1 = pack('H*', $data1);
	echo("MD5: ".$data1."<br />");
	$data1 = base64_encode($data1);
	echo("MD5: ".$data1."<br />");
	$data1 = md5($data, true);
	echo("MD5: ".$data1."<br />");
	$data1 = base64_encode($data1);
	echo("MD5: ".$data1."<br />");
	$data1 = sha1($data, true);
	echo("SHA1: ".$data1."<br />");
	$data1 = base64_encode($data1);
	echo("SHA1: ".$data1."<br />");
	return $data;
}
function createPasswords($user, $service, $pass, $gen)
{
	$passWords = '';

	$espChars = array("!", "@", "#", "/", "&");


	// Elementos
	$servCheapPass = "";
	$userCheapPass = "";
	$passCheapPass = "";
	$numberEnd = "";
	$slenuser = strlen($user);
	$slenserv = strlen($service);
	$slenpass = strlen($pass);

	// Datos sobre Servicio
	$servW = explode(" ", $service);
	$nServW = count($servW);
	foreach ( $servW as $x )	
	{
		$servCheapPass.= $x[0].strtoupper($x[1]);
		$numberEnd.= strval((strlen($x)+1)*3);
	}

	// Datos sobre Usuario
	if ( $slenuser >= 4 )
	{
		$userCheapPass = $user[0].strtoupper($user[1]).strtoupper($user[$slenuser-1]).$user[$slenuser-2];
	} else {
		$userCheapPass = $user[0].strtoupper($user[1]);
	}
	$numberEnd.= strval(($slenuser+2)*3);

	// Datos sobre la contrase�a
	if ( $slenpass >= 4 )
	{
		$passCheapPass = strtoupper($pass[0]).$pass[$slenpass-2].strtoupper($pass[$slenpass-1]).$pass[1];
	} else {
		$passCheapPass = strtoupper($pass[0]).$pass[1];
	}
	$numberEnd.= strval(($slenpass)*7);

	// Conversiones finales
	$numberEnd = $gen*$numberEnd;
	$genB36 = base_convert($gen, 10, 36);
	$numberEndB36 = base_convert($numberEnd, 10, 36);

	// Encriptaciones
	$serviceMD5 = md5_base64("g".$genB36.$service.$pass);
	$userMD5 = md5_base64("g".$genB36.$user.$pass);
	$numberMD5 = md5_base64($numberEndB36);
	$hybrid1 = md5_base64($servCheapPass.$passCheapPass);
	$hybrid2 = md5_base64($userCheapPass.$servCheapPass.$passCheapPass);

	// PW 1
	$p_b_wu_s16_1 = $servCheapPass.$slenserv.$userCheapPass.$slenuser.$passCheapPass.$slenpass.$genB36."mP".$numberEndB36;
	$p_s_wu_s24_1 = substr($numberMD5, 8, 6).substr($serviceMD5, 6, 6).substr($userMD5, 3, 6).substr($hybrid2, 6, 6);
	$p_s_wu_s22_1 = substr($numberMD5, 2, 6).substr($serviceMD5, 1, 6).substr($userMD5, 7, 6).substr($hybrid2, 0, 4);
	$p_s_wu_s16_1 = substr($numberMD5, 6, 3).substr($serviceMD5, 4, 3).substr($userMD5, 4, 7).substr($hybrid2, 0, 3);
	$p_s_wu_s12_1 = substr($numberMD5, 0, 3).substr($serviceMD5, 2, 3).substr($userMD5, 9, 3).substr($hybrid2, 9, 3);
	$p_s_wu_s08_1 = substr($numberMD5, 3, 2).substr($serviceMD5, 2, 2).substr($userMD5, 4, 2).substr($hybrid2, 0, 2);


	// PW 2
	$p_b_wu_s16_2 = preg_replace('/[^A-Za-z0-9]/', '', $p_b_wu_s16_1);
	$p_s_wu_s24_2 = preg_replace('/[^A-Za-z0-9]/', '', $p_s_wu_s24_1);
	$p_s_wu_s22_2 = preg_replace('/[^A-Za-z0-9]/', '', $p_s_wu_s22_1);
	$p_s_wu_s16_2 = preg_replace('/[^A-Za-z0-9]/', '', $p_s_wu_s16_1);
	$p_s_wu_s12_2 = preg_replace('/[^A-Za-z0-9]/', '', $p_s_wu_s12_1);
	$p_s_wu_s08_2 = preg_replace('/[^A-Za-z0-9]/', '', $p_s_wu_s08_1);


	// PW L 1
	$pl_b_wu_s16_1 = strlen($p_b_wu_s16_1);
	$pl_s_wu_s24_1 = strlen($p_s_wu_s24_1);
	$pl_s_wu_s22_1 = strlen($p_s_wu_s22_1);
	$pl_s_wu_s16_1 = strlen($p_s_wu_s16_1);
	$pl_s_wu_s12_1 = strlen($p_s_wu_s12_1);
	$pl_s_wu_s08_1 = strlen($p_s_wu_s08_1);


	// PW L 2
	$pl_b_wu_s16_2 = strlen($p_b_wu_s16_2);
	$pl_s_wu_s24_2 = strlen($p_s_wu_s24_2);
	$pl_s_wu_s22_2 = strlen($p_s_wu_s22_2);
	$pl_s_wu_s16_2 = strlen($p_s_wu_s16_2);
	$pl_s_wu_s12_2 = strlen($p_s_wu_s12_2);
	$pl_s_wu_s08_2 = strlen($p_s_wu_s08_2);

$passWords = <<<EOD
<table class="qTable">
	<caption>True SET</caption>
	<thead><tr><th>T</th><th>B64</th><th>L</th><th>B61</th><th>L</th></tr></thead>
	<tbody>
		<tr><td>b_16</td><td>$p_b_wu_s16_1</td><td>$pl_b_wu_s16_1</td><td>$p_b_wu_s16_2</td><td>$pl_b_wu_s16_2</td></tr>
		<tr><td>s_24</td><td>$p_s_wu_s24_1</td><td>$pl_s_wu_s24_1</td><td>$p_s_wu_s24_2</td><td>$pl_s_wu_s24_2</td></tr>
		<tr><td>s_22</td><td>$p_s_wu_s22_1</td><td>$pl_s_wu_s22_1</td><td>$p_s_wu_s22_2</td><td>$pl_s_wu_s22_2</td></tr>
		<tr><td>s_16</td><td>$p_s_wu_s16_1</td><td>$pl_s_wu_s16_1</td><td>$p_s_wu_s16_2</td><td>$pl_s_wu_s16_2</td></tr>
		<tr><td>s_12</td><td>$p_s_wu_s12_1</td><td>$pl_s_wu_s12_1</td><td>$p_s_wu_s12_2</td><td>$pl_s_wu_s12_2</td></tr>
		<tr><td>s_08</td><td>$p_s_wu_s08_1</td><td>$pl_s_wu_s08_1</td><td>$p_s_wu_s08_2</td><td>$pl_s_wu_s08_2</td></tr>
	</tbody>
</table>
EOD;
	//
	return $passWords;	
}
function createPasswords2($user, $service, $pass, $gen)
{
	$modded = [$user, $service, $pass];
	for ( $i = 0; $i < count($modded); $i++ )
	{
		$mod = $modded[$i];
		$modNumber = $gen * strlen($mod) + $gen;
		$stringHalf = round(strlen($mod) / 2);
		$modded[$i] = substr($mod, $stringHalf).$modNumber.substr($mod, 0, $stringHalf);
	}
	// Modificaciones
	$newMod = [];
	$newMod[] = $modded[0].$modded[1].$modded[2];
	$newMod[] = $modded[1].$modded[2].$modded[0];
	$newMod[] = $modded[0].$modded[2].$modded[1];
	$newMod[] = $user.$service.$pass;
	//
	for ( $i = 0; $i < count($newMod); $i++ )
	{
		$newMod[$i] = sha1_base64_cleaned($newMod[$i]);
	}

	$list = '';
	$pos = 0;
	for ( $i = 2; $i < 7; $i++ )
	{
		$key = '';
		$charsTot = 4 * $i;
		foreach( $newMod as $mod )
		{
			if ( strlen($mod) > ($pos + $i) ) {
				$key.= substr($mod, $pos, $i);
			} else {
				$key.= substr($mod, -$i, $i);
			}
		}
		$pos += $i;
		$charsMod = strlen($key);
		$list.= "<tr><td>{$charsTot}</td><td>{$charsMod}</td><td>{$key}</td></tr>";
	}
	
	$passWords = <<<EOD
<table class="qTable">
<caption>True SET</caption>
<thead><tr><th>Count (E)</th><th>Count (T)</th><th>Key</th></tr></thead>
<tbody>
{$list}
</tbody>
</table>
EOD;

	//
	return $passWords;
}


function createFakePasswords($user, $service, $pass, $gen)
{
	$genB36 = base_convert($gen, 10, 36);
	$constant = md5_base64($user.$service.$pass);
	$serviceMD5 = md5_base64("g".$genB36.$service.$pass);
	$userMD5 = md5_base64("g".$genB36.$user.$pass);

	// PW 1
	$p_s_wu_s16_1 = substr($serviceMD5, 2, 6).substr($userMD5, 1, 4).substr($constant, 0, 4);
	$p_s_wu_s12_1 = substr($serviceMD5, 5, 4).substr($userMD5, 4, 4).substr($constant, 4, 4);
	$p_s_wu_s08_1 = substr($serviceMD5, 4, 3).substr($userMD5, 9, 3).substr($constant, 8, 2);

	// PW 2
	$p_s_wu_s16_2 = preg_replace('/[^A-Za-z0-9]/', '', $p_s_wu_s16_1);
	$p_s_wu_s12_2 = preg_replace('/[^A-Za-z0-9]/', '', $p_s_wu_s12_1);
	$p_s_wu_s08_2 = preg_replace('/[^A-Za-z0-9]/', '', $p_s_wu_s08_1);

	// PW L 1
	$pl_s_wu_s16_1 = strlen($p_s_wu_s16_1);
	$pl_s_wu_s12_1 = strlen($p_s_wu_s12_1);
	$pl_s_wu_s08_1 = strlen($p_s_wu_s08_1);

	// PW L 2
	$pl_s_wu_s16_2 = strlen($p_s_wu_s16_2);
	$pl_s_wu_s12_2 = strlen($p_s_wu_s12_2);
	$pl_s_wu_s08_2 = strlen($p_s_wu_s08_2);
	
	
$passWords = <<<EOD
<table class="qTable">
	<caption>Passwords</caption>
	<thead><tr><th>T</th><th>B64</th><th>L</th><th>B61</th><th>L</th></tr></thead>
	<tbody>
		<tr><td>s_16</td><td>$p_s_wu_s16_1</td><td>$pl_s_wu_s16_1</td><td>$p_s_wu_s16_2</td><td>$pl_s_wu_s16_2</td></tr>
		<tr><td>s_12</td><td>$p_s_wu_s12_1</td><td>$pl_s_wu_s12_1</td><td>$p_s_wu_s12_2</td><td>$pl_s_wu_s12_2</td></tr>
		<tr><td>s_08</td><td>$p_s_wu_s08_1</td><td>$pl_s_wu_s08_1</td><td>$p_s_wu_s08_2</td><td>$pl_s_wu_s08_2</td></tr>
	</tbody>
</table>
EOD;
	return $passWords;
}
// }
// =====================================
//	FUNCIONES
// =====================================

$passWords = '';
if ( !empty($_POST['submit']) )
{
	$pwcPw = cleanData($_POST['var0']);
	$user = cleanData($_POST['var1']);
	$service = cleanData($_POST['var2']);
	$pass = cleanData($_POST['var3']);
	$gen = intval($_POST['var4']);
	if ( $pwcPw === '5553970382' )
	{
		$passWords.= createPasswords($user, $service, $pass, $gen);
		$passWords.= createPasswords2($user, $service, $pass, $gen);
	} else {
		$passWords = createFakePasswords($user, $service, $pass, $gen);
	}
}


header('Content-Type: text/html; charset=utf-8');
echo <<<EOD
<!DOCTYPE HTML>
<html>
<head>
<meta name="viewport" content="width=device-width">
<title>Snk-pwc</title>
</head>
<style type="text/css">
body { font-size: 90%; width: 480px; margin: 0; padding: 0; border: 0; font-family: "Courier New", Courier, monospace; }
.qTable { background-color: #000000; border-spacing: 1px }
.qTable thead { background-color:#FFF; }
.qTable thead th { padding: 5px; text-align: center; }
.qTable tbody { background-color: #FFFFFF; }
.qTable tbody td { background-color: #FFFFFF; padding: 5px; }
</style>
<body>
<form method="post">
<table>
<thead><tr><th>C</th><th>T</th></thead>
<tbody>
<tr><td>C:</td><td><input type="text" size="40" name="var0" value="{$_POST['var0']}" /></td></tr>
<tr><td>U:</td><td><input type="text" size="40" name="var1" value="{$_POST['var1']}" /></td></tr>
<tr><td>S:</td><td><input type="text" size="40" name="var2" value="{$_POST['var2']}" /></td></tr>
<tr><td>P:</td><td><input type="text" size="40" name="var3" value="{$_POST['var3']}" /></td></tr>
<tr><td>G:</td><td><input type="text" size="40" name="var4" value="{$_POST['var4']}" /></td></tr>
<tr><td></td><td><input type="submit" name="submit" value="Aceptar" /></td></tr>
</tbody>
</table>
</form>
{$passWords}
</body>
</html>
EOD;
//
?>