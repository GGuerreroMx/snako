<?php
$page['head']['metaDesc'] = "An open source asyncronic Web Developer Framework, focused in rapid design and websites optimizations.";

//
// Página
$page['body'] = <<<HTML
<style>
.index_page { padding:30px 0; }
.index_page.main { height:calc( 100vh - 45px); padding:0; }
.snkbkg { height:100%; margin-top:30px; margin-bottom:30px; position:relative;
	background:url(/snkeng/site/res/img/st_logo.svg) top right / 65vh auto no-repeat; }
.index_page_desc { position:absolute; top:70px; left:70px; width:400px; height:400px; }
.index_page_desc .title { font-size:3.2rem; font-weight:bold; }
.index_page_desc .desc { font-size:1.8rem; }
/* */
.index_page h1 { font-size:3.4rem; text-align:center; margin-bottom:30px; }
/* */
.feature_list { display:flex; flex-direction:row; flex-wrap:wrap; justify-content:space-around; }
.index_feature { width:25%; margin:0 3% 20px; }
.index_feature .icon { border-radius:50%; height:100px; width:100px; display:flex; align-items:center; justify-content:center; border:2px solid #D80018; color:#333; margin:0 auto 20px auto; }
.index_feature .icon svg { height:50%; width:50%; fill:#D80018; }
.index_feature .title { font-weight:bold; text-align:center; margin-bottom:20px; }
.index_feature .content { text-align:justify; font-size:1.4rem; line-height:1.7; }
/* */
.puq_question { margin-bottom:20px; }
.puq_question .question { font-weight:bold; margin-bottom:10px; }
.puq_question .answer {}
/* Modo celular */
@media only screen and (max-width:767px) {
	.index_page.main { height:calc( 100vh - 55px); }
	.index_page_desc { left:0; bottom:0; top:auto; width:auto; height:auto; background-color:rgba(255,255,255,0.85); border-radius:5px; }
	.snkbkg { margin-top:0; margin-bottom:0; background-size:100% auto; }
	.feature_list { display:block; }
	.index_feature { width:100%; margin:0 0 20px; }
}
/* Modo tablet */
@media only screen and (min-width:768px) and (max-width:1024px) {
	.index_page { }
}
</style>
<div class="index_page main">
<div class="wpContent snkbkg">

<div class="index_page_desc">
<div class="title">Snako Engine</div>
<div class="desc">An open source WDF designed for developers, build to create asyncronic and optimized websites.</div>

</div>
</div>
</div>
<div class="index_page">
<div class="wpContent">
<h1>Features</h1>
<div class="feature_list">
	<div class="index_feature">
	<div class="icon"><svg><use xlink:href="#fa-server"></use></svg></div>
	<div class="title">Smart server behavior</div>
	<div class="content">Server behaves differently when it is defined as a testing server or as a production server and it's customizable too.</div>
	</div>
	<div class="index_feature">
	<div class="icon"><svg><use xlink:href="#fa-wrench"></use></svg></div>
	<div class="title">Built in administrative tools</div>
	<div class="content">Design de databases, read and delete the data from within the framework, merge, minify and compress JS, CSS and SVG files with a single click.</div>
	</div>
	<div class="index_feature">
	<div class="icon"><svg><use xlink:href="#fa-refresh"></use></svg></div>
	<div class="title">Easy Deployment</div>
	<div class="content">Sincronize your testing server with your production one with a single click.</div>
	</div>
	<div class="index_feature">
	<div class="icon"><svg><use xlink:href="#fa-code"></use></svg></div>
	<div class="title">Custom JS Library</div>
	<div class="content">The built in JS library handle all the async behavior, enables plugins and creates a DOM shorthand by prototype extention.</div>
	</div>
	<div class="index_feature">
	<div class="icon"><svg><use xlink:href="#fa-css3"></use></svg></div>
	<div class="title">Custom CSS Library</div>
	<div class="content">Designed with the latest CSS, the base library solve many of the base necesities for designing.</div>
	</div>
	<div class="index_feature">
	<div class="icon"><svg><use xlink:href="#fa-gears"></use></svg></div>
	<div class="title">Make it your way</div>
	<div class="content">You can edit the behavior of the framework to adapt to your workstyle, from de PHP coding, to the CSS and JS. It's designed with simplicity in mind.</div>
	</div>
</div>
</div>
</div>
<div class="index_page">
<div class="wpContent">
<h1>Possible Unasqued Questions (P.U.Q)</h1>
<div class="grid">
<div class="gr_sz06 gr_ps03">
	<div class="puq_question">
		<div class="question">Why it was developed?</div>
		<div class="answer">It was originally created as a playground and used for very simple projects. As those projects grew, so did the "Framework".</div>
	</div>
	<div class="puq_question">
		<div class="question">What kind of logic is involved in creating web apps?</div>
		<div class="answer">Each "feature" its called an App (ex. a "blog", "static pages", "store"). This apps may share database tables, or may contain exclusive elements of their own. Codewise each app may contain a "page" behaviour, an "ajax" behaviour and a module (within php) callings. Outside that basic principles, the app may be build in any way within the code.</div>
	</div>
	<div class="puq_question">
		<div class="question">What kind of browser support does it have?</div>
		<div class="answer">I have a very profound feeling against enabling support for older browsers. But at the same time I try to support as many vendors as possible. With that in mind I will only ensure 100% working with the 2 latest browser rule or a yearly release rule whichever is lower.</div>
	</div>
	<div class="puq_question">
		<div class="question">I'm interested in testing, where can I download it?</div>
		<div class="answer">I want to convert the framework into an open source code, that doesn't mean its yet available. Code will be released at stages with some kind of documentation so I'm not wasting your time. I'll first release the JS and CSS.</div>
	</div>
	<div class="puq_question">
		<div class="question">Is the current sitez built on this framework?</div>
		<div class="answer">Yes</div>
	</div>
	<div class="puq_question">
		<div class="question">Does this framework support templates?</div>
		<div class="answer">No, the idea behind the framework is to provide a very "thin" layer between the "codes", so nothing is 100% automatic but at the same time not building everything from scratch. By this logic the templates behaviour is non-existent, but it is designed so it's easy to make modifications and optimizations.</div>
	</div>
	<div class="puq_question">
		<div class="question">Is the code ready for production?</div>
		<div class="answer">Many parts of the code are very optimized, and run very smoothly, so for certain kind of applications I believe its a nice alternative.</div>
	</div>
</div>
</div>
</div>
</div>\n
HTML;
