<?php
$page['header']['title'] = 'Cuenta bloqueada';
$page['body'] = <<<HTML
<div class="fullTitle">
	<div class="wpContent"><h1>Cuenta Bloqueada</h1></div>
</div>
<div class="section">
<div class="wpContent">
El acceso al sitio se encuentra bloqueado para esta cuenta.<br />
<a se-nav="se_middle" href="/?logout=true">Cerrar Sesión.</a>
</div>
</div>
HTML;
