<?php
$page['header']['title'] = '404';
$page['body'] = <<<HTML
<div class="fullTitle"><div class="wpContent"><h1>404: Página no encontrada</h1></div></div>

<div class="section">
<div class="wpContent">
La página introducida (URL) no es válida. Favor de verificarla.<br />
<a se-nav="se_middle" href="/">Ir a la página frontal.</a>
</div>
</div>
HTML;
