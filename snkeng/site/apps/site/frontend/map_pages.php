<?php
$conditions['app']['name'] = 'site';

// debugVariable($conditions['app']['site']);

//
$firstParam = ( \se_nav::current() === $conditions['app']['site']['settings']['blog']['name'] ) ? 'blog' : \se_nav::current();

//
switch ( $firstParam ) {
	//
	case 'docs':
		require __DIR__ . '/pages/docs_a_nav.php';
		break;

	//
	case 'post':
	case 'videos':
	case 'blog':
		require __DIR__ . '/pages/blog_a_nav.php';
		break;

	//
	case 'contact':
		require __DIR__ . '/pages/contact.php';
		break;

	//
	default:
		$cUrl = $_SERVER['REQUEST_URI'];
		// Remover parámetros get
		if ( strpos($cUrl, '?') !== false ) {
			$cUrl = substr($cUrl, 0, strpos($cUrl, '?'));
		}

		// Ajustar si es async
		if ( \se_nav::$async ) {
			$cUrl = substr($cUrl, strpos($cUrl, '/', 9));
		}

		// Security
		$cUrl = preg_replace('/[^A-Za-z0-9\/\-_]/', '', $cUrl);

		// Revisar que exista contenido
		if ( empty($cUrl) ) { \se_nav::invalidPage(); }

		// Obtener página a partir del nombre
		$sql_qry = <<<SQL
SELECT
	art_id AS id, art_title AS title, art_urltitle AS urlTitle, art_url AS fullUrl,
    art_type_sub AS typeSub, art_content_extra_b AS extraBasic,
	art_meta_word AS metaWord, art_meta_desc AS metaDesc, art_content AS content
FROM sc_site_articles
WHERE art_url='{$cUrl}' AND art_type='page'
LIMIT 1;
SQL;
		$params['page'] = $mysql->singleRowAssoc(
			$sql_qry,
			[],
			[
				'errorKey' => 'AppSitesMap01',
				'errorDesc' => 'No fue posible revisar por sitios externos'
			]
		);

		// Hay datos del sitio?
		if ( empty($params['page']) ) { \se_nav::invalidPage(); }

		// debugVariable($params['page']);

		// Redirection page?
		if ( $params['page']['typeSub'] === 'redir' ) {
			$extraLink = '';

			// Check if it is a partial page
			if ( substr( $_SERVER['REQUEST_URI'], 0, 8) === '/partial' ) {
				$extraLink = substr( $_SERVER['REQUEST_URI'], 0, strpos($_SERVER['REQUEST_URI'], '/', 9));
			}

			// Redirect
			header("Location: {$extraLink}{$params['page']['extraBasic']}", true, 307);
			exit();
		}

		// Ejecutar página
		require __DIR__ . '/pages/page_simple.php';
		break;
}
//
