<?php

//
$sql_qry = <<<SQL
SELECT
	docs.docs_id AS id,
	docs_title_normal AS title, docs_title_url AS urlTitle,
	docs_description AS dDesc, docs_content AS content,
	docs_img_struct AS image
FROM sc_site_documents AS docs
WHERE docs.docs_id={$params['docData']['id']};
SQL;
$docData = $mysql->singleRowAssoc($sql_qry, [
	'int' => ['id']
]);

//
$page['head']['title'] = $docData['title'];
$page['head']['metaDesc'] = $docData['dDesc'];
// Image
$page['head']['og']['img'] = $siteVars['server']['url'].'/server/image/site/w_832/'.$docData['image'].'.jpg';


//
$sql_qry = <<<SQL
SELECT
	art.art_id AS id,
	art.art_title AS title, art.art_urltitle AS urlTitle, art.art_url AS fullUrl,
	CONV(art.art_order_current , 36, 10) AS orderCur, art.art_order_level AS orderLvl,
	art.art_order_hierarchy AS orderHierarchy
FROM sc_site_articles AS art
WHERE art.art_type='docs' AND art.cat_id={$params['docData']['id']} AND art.art_published=1
ORDER BY art.art_order_hierarchy ASC
LIMIT 150;
SQL;


$struct = <<<HTML
<a se-nav="se_middle" data-level="!orderLvl;" data-order="!orderCur;" href="!fullUrl;">!title;</a>
HTML;

$content = $mysql->printSimpleQuery($sql_qry, $struct);


// Docs - Navegación
$page['head']['title'] = 'Documentation';
// Página
$page['body'] = <<<HTML
<div class="fullTitle"><div class="wpContent">{$docData['title']}</div></div>
<!-- INI:Documentation -->
<div class="wpContent grid docMain">
	<!-- INI:POSTS -->
	<div class="gr_sz08">
		<h2 class="title">Table of contents</h2>
		<div class="docsMenu docs">{$content}</div>
	</div>
	<div class="gr_sz04">
		<img class='mainImg' itemprop='image' src='/server/image/site/w_320/{$docData['image']}' />
		<div class="content">{$docData['content']}</div>
	</div>
	<!-- END:POSTS -->
</div>
<!-- END:BLOG -->\n
HTML;
