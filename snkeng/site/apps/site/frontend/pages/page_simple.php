<?php
// Adición de título, desc y tags.
$page['head']['title'] = $params['page']['title'];
$page['head']['metaWord'] = $params['page']['metaWord'];
$page['head']['metaDesc'].= $params['page']['metaDesc'];
$page['head']['url'] = $params['page']['fullUrl'];
$page['head']['og']['type'] = 'website';


// debugVariable($params['page']);
if ( !empty($params['page']['imageStruct']) ) {
	$page['head']['og']['img'] = $siteVars['server']['url'].'/server/image/site/w_720/'.$params['page']['imageStruct'].'.jpg';
}

//
switch ( $params['page']['typeSub'] ) {
	//
	case 'image':
		$mainImage = '';

		// debugVariable($params['page']);
		if ( !empty($params['page']['imageStruct']) ) {
			//
			$mainImage = <<<HTML
<div class="pageCont pageMediaContainer">
	<picture itemprop="image">
		<source type="image/webp"
			srcset="/server/image/site/w_360/{$params['page']['imageStruct']}.webp 360w,
					/server/image/site/w_720/{$params['page']['imageStruct']}.webp 720w,
					/server/image/site/w_1200/{$params['page']['imageStruct']}.webp 1200w"
			sizes="(max-width:768px) 100vw, 80vw"
		/> 
		<img class="mainImg" alt="{$params['page']['imageAltText']}" width="{$params['page']['imageWidth']}" height="{$params['page']['imageHeight']}"
			src="/server/image/site/w_1200/{$params['page']['imageStruct']}.jpg"
			srcset="/server/image/site/w_360/{$params['page']['imageStruct']}.jpg 360w,
					/server/image/site/w_720/{$params['page']['imageStruct']}.jpg 720w,
					/server/image/site/w_1200/{$params['page']['imageStruct']}.jpg 1200w"
			sizes="(max-width:768px) 100vw, 80vw"
		/>
	</picture>
</div>
HTML;
			//
		}

		//
		$page['body'] = <<<HTML
<div class="fullTitle"><div class="wpContent">{$params['page']['title']}</div></div>
{$mainImage}
<!-- INI:PAGE CONTENT -->
<div class="pageSimple_text adv_text">
{$params['page']['content']}
</div>
<!-- END:PAGE CONTENT -->\n
HTML;
//
		break;
	//
	case 'text':
		//
		$page['body'] = <<<HTML
<div class="fullTitle"><div class="wpContent">{$params['page']['title']}</div></div>
<!-- INI:PAGE CONTENT -->
<div class="pageSimple_text adv_text">
{$params['page']['content']}
</div>
<!-- END:PAGE CONTENT -->\n
HTML;
//
		break;

	// Default full html behaviour
	case 'normal':
	case '':
	case null:
		$adminData = '';
		//
		if ( se_login::check_loginLevel('admin') )
		{
			\se_nav::pageFileGroupAdd(['admin_editHTML']);
			//
			$adminData = " se-plugin=\"wysiwyg\" data-saveurl=\"/ajax/admin/core/site/pages/content/updLive\" data-id=\"{$params['page']['id']}\"";
		}
		//
		$page['body'] = <<<HTML
<div class="fullTitle"><div class="wpContent">{$params['page']['title']}</div></div>
<div id="editCont" class="adv_text"{$adminData}>{$params['page']['content']}</div>\n
HTML;
//
		break;

	//
	default:
		se_killWithError('caso no programado');
		break;
}


