<?php
// BLOG - Post
// debugVariable($conditions['app']['site']['settings']['blog']);

//
// TAGS
/*
$sql_qry = "SELECT tag_title, tag_urltitle
			FROM sc_site_tags AS tlist
			INNER JOIN st_post_tagrel AS trel ON tlist.tag_id=trel.tag_id
			WHERE trel.p_id={$params['page']['pId']}
			LIMIT 10;";
$first = true;
$tags = "";
$tagsUrl = "";
if ( $mysql->execQuery($sql_qry) )
{
	while ( $datos = $mysql->result->fetch_array() )
	{
		$tags.= ($first) ? "" : ", ";
		$tagsUrl.= ($first) ? "" : ", ";
		$first = false;
		$tags.= htmlentities($datos[0]);
		$tagsUrl.= '<a onClick="return se_ajaxpage(this, \'se_middle\');" href="{$siteVars['server']['base_path']}/tags/'.$datos[1].'/">'.htmlentities($datos[0]).'</a>';
	}
} else {  $mysql->killIfError("No se pudieron leer los tags del post.", "full"); }
*/

\se_nav::cacheCheckFile(__FILE__);


//
// Datos post
$sql_qry = <<<SQL
SELECT
	art.art_id AS pId, art.art_title AS pTitle, art.art_urltitle AS purltitle, art.art_url AS fullUrl,
	art.art_meta_desc AS pdesc, art.art_meta_word AS pword, art.art_content AS pcontent,
	art.art_type_sub AS typeSub, art.art_content_extra AS contentExtra, art.art_content_extra_b AS contentExtraB,
	art.art_likes AS plikes, art.art_nolikes AS pnolikes,
	DATE_FORMAT(art.art_dt_pub, '%e %M %Y') AS dtpub_simple, DATE_FORMAT(art.art_dt_pub, '%Y-%m-%dT%TZ') AS dtpub_iso,
	DATE_FORMAT(art.art_dt_mod, '%e %M %Y %T') AS dtmod_simple, DATE_FORMAT(art.art_dt_mod, '%Y-%m-%dT%TZ') AS dtmod_iso,
	art.com_id AS comId,
    art.art_img_struct AS imgFileName, art.art_img_alt_text AS imageAltText, art.art_img_width AS imgWidth, art.art_img_heigth AS imgHeigth,
	t2.cat_title AS cname, t2.cat_urltitle AS curlname,
	t3.a_id AS userId, t3.a_obj_name AS username, t3.a_url_name_full AS userurl, t3.a_img_crop AS usrImage
FROM sc_site_articles AS art
LEFT OUTER JOIN sc_site_category AS t2 ON t2.cat_id=art.cat_id
LEFT OUTER JOIN sb_objects_obj AS t3 ON t3.a_id=art.a_id
WHERE art_id={$params['page']['pId']}
LIMIT 1;
SQL;
$curPostObj = $mysql->singleRowAssoc($sql_qry,
	[
		'int' => ['pId']
	],
	[
		'errorKey' => 'blog_single_load',
		'errorDesc' => 'No fue posible leer la información del post',
		'emptyDesc' => 'No hay datos'
	]
);

// Contador de visitas simple
if ( !se_login::check_loginLevel('admin') && empty($_SESSION['views']['art'][$params['page']['pId']]) ) {
	$_SESSION['views']['art'][$params['page']['pId']] = 1;
	$sql_qry = "UPDATE sc_site_articles SET art_views=art_views+1 WHERE art_id={$params['page']['pId']} LIMIT 1;";
	if ( !$mysql->submitQuery($sql_qry) ) { echo("ERROR (SISTEMA): contador de visitas."); }
}

\se_nav::cacheFinalCheck();

// Sitio General
$page['head']['title'] = $curPostObj['pTitle'];
$page['head']['metaWord'] = $curPostObj['pword'];
$page['head']['metaDesc'] = $curPostObj['pdesc'];
// URL
$page['url_path'] = $curPostObj['fullUrl'];
$page['head']['url'] = $siteVars['server']['url'].$curPostObj['fullUrl'];
//
$page['head']['og']['img'] = $siteVars['server']['url'].'/server/image/site/w_832/'.$curPostObj['imgFileName'].'.jpg';
$page['head']['og']['type'] = "article";

//
$curPostObj['user_url'] = "/blog/author/{$curPostObj['userurl']}/";

//<editor-fold desc="Open graph">

// Open Graph
if ( isset($siteVars['sn']['fb']['page']['id']) ) {
	$page['head']['og']['data'][] = ['og', 'article:publisher', $siteVars['sn']['fb']['page']['id']];
}
$page['head']['og']['data'][] = ['og', 'article:published_time', $curPostObj['dtpub_iso']];
if ( !empty($curPostObj['dtmod_iso']) ) { $page['head']['og']['data'][] = ['og', 'article:modified_time', $curPostObj['dtmod_iso']]; }
$page['head']['og']['data'][] = ['og', 'article:section', $curPostObj['cname']];
// Twitter
$page['head']['og']['twType'] = 'summary_large_image';

// AMP Support
$page['head']['extra'][] = [
	'type' => 'link',
	'content' => "rel='amphtml' href='{$siteVars['site']['url']}{$curPostObj['fullUrl']}?amp=1'"
];
//</editor-fold>

//<editor-fold desc="Comentarios">

//
$commentsHTML = '';
$commentNumber = 0;
switch ( $conditions['app']['site']['settings']['blog']['comments'] )
{
	// Red social
	case 1:
		if ( !$siteVars['site']['modules']['socnet'] ) { die('Red social no habilitada'); }
		// Obtener Objeto para crear comentarios
		$commentsObj = getClassMod('sn_comments');
		// Analizar si ya se tiene vinculado un comentario
		$curPostObj['comId'] = intval($curPostObj['comId']);
		if ( $curPostObj['comId'] === 0 )
		{
			// Variables
			$parentId = $curPostObj['pId'];
			$notifName = "Post - {$curPostObj['pTitle']}";
			$notifURL = $page['url_path'].'#comments';
			// Crear
			$curPostObj['comId'] = $commentsObj->comObjAdd(1, 1, 0, $parentId, $notifName, $notifURL);
			// Actualizar
			$sql_qry = "UPDATE st_post_object SET com_id={$curPostObj['comId']} WHERE p_id={$curPostObj['pId']};";
			$mysql->submitQuery($sql_qry, [
				'errorKey' => 'postSingle01Com',
				'errorDesc' => 'No fue posible actualizar el post con los comentarios.'
			]);
		}
		// Obtener propiedades del comment
		$commentsObj->comObjSet($curPostObj['comId']);
		// Cuenta de comentarios
		$commentNumber = $commentsObj->getCommentCount($curPostObj['comId']);

		// Generar HTML
		$commentsHTML.= <<<HTML
<h2><a name="comments">{$postLang['comentaries']}</a></h2>\n
HTML;
		// Imprimir sistema de comentarios
		$commentsObj->setupPublicComments(true);
		$commentsHTML.= $commentsObj->response['html'];
		$page['mt'].= $commentsObj->response['mt'];
		$page['js'].= $commentsObj->response['js'];
		break;

	// Disqus
	case 2:
		if ( empty($siteVars['extmod']['disqus']) ) { die('DISQUS no configurado.'); }
		$commentsHTML.= <<<HTML
<h2><a name="comments">{$postLang['comentaries']}</a></h2>\n
HTML;
		if ( !$siteVars['server']['test'] )
		{
			// AJS
			$page['files']['js'][] = "//{$siteVars['extmod']['disqus']['shortname']}.disqus.com/embed.js";

			// HTML
			$commentsHTML.= <<<HTML
<div id="disqus_thread"></div>
<noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
<a href="http://disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>
HTML;
			// JS
			$page['mt'].=<<<JS
DISQUS.reset({
    reload: true,
    config: function () {
        this.page.identifier = 'bx_{$params['page']['pId']}';
        this.page.url = '{$page['urlFull']}';
        this.page.title = '{$page['head']['title']}';
    }
});
JS;
		} else {
			$commentsHTML.= "<div class='warning'>Disqus listo, pero no se muestra en servidores de prueba.</div>\n";
		}
		break;

	// Facebook
	case 3:
		if ( !$siteVars['server']['test'] )
		{
			$page['files']['js'][] = "//connect.facebook.net/es_MX/sdk.js#xfbml=1&version=v2.8&appId=".$siteVars['sn']['fb']['app']['id'];
			// HTML
			$commentsHTML.= <<<HTML
<div class="fb-comments" data-href="{$page['head']['url']}" data-width="100%" data-numposts="5" data-colorscheme="light"></div>
HTML;
		} else {
			$commentsHTML.= "<div class='warning'>Facebook Comments listo, pero no se muestra en servidores de prueba.</div>\n";
		}
		break;

	// Sin comentarios
	default:
	case 0:
		break;
}

//</editor-fold>

//<editor-fold desc="UserFocus">
$userBottom = '';
if ( $conditions['app']['site']['settings']['blog']['bloggerFocus'] )
{
	// Read user settings
	$userData = snkeng\core\socnet\object_properties::readArray($curPostObj['userId'], 'snkeng', 'profile');
	if ( empty($userData) ) { se_killWithError('User information not set'); }
	$userData = $userData['data'];

	//
	$userDesc = '';

	$socnet = '';
	if ( !empty($userData) ) {
		// Twitter
		if ( $userData['sn_tw_id'] ) {
			$page['head']['og']['data'][] = ['twitter', 'twitter:creator:id', $userData['sn_tw_id']];
		}
		if ( $userData['sn_tw_nm'] ) {
			$page['head']['og']['data'][] = ['twitter', 'twitter:creator', $userData['sn_tw_nm']];
			$socnet.= "<a target='_blank' href='https://twitter.com/{$userData['sn_tw_nm']}' data-rel='author'><svg class='icon inline'><use xlink:href='#fa-twitter'></use></svg></a>";
		}
		// Facebook
		if ( $userData['sn_fb_id'] ) {
			$page['head']['og']['data'][] = ['og', 'article:author', $userData['sn_fb_id']];
			$socnet.= "<a target='_blank' href='https://www.facebook.com/{$userData['sn_fb_nm']}'><svg class='icon inline'><use xlink:href='#fa-facebook'></use></svg></a>";
		}
		// Descripción
		$userData = $userData['desc'];
	}

	//
	$userBottom.= <<<HTML
<div class="blog_user">
	<img class="userImg" src="/server/image/objects/w_120/{$curPostObj['usrImage']}" />
	<div class="uData"><div class="title">{$curPostObj['username']}</div><div class="desc">{$userDesc}</div><div class="socnet">{$socnet}</div></div>
</div>
HTML;
	//
}
//</editor-fold>

//<editor-fold desc="SideBars">
$sideBarHTML = snkeng\core\site\sideBar::createSidebar([
	['name'=>'blogSearch', 'props'=>''],
	['name'=>'blogMosts', 'props'=>''],
	['name'=>'blogCategory', 'props'=>''],
	['name'=>'banner', 'props'=> [ 'size' => 2 ] ],
]);

//</editor-fold>

//<editor-fold desc="Likes">

// Likes
$likeString = ( empty($_SESSION['c'.$params['page']['pId']]) ) ?
	"<button class='likeBtn' se-act='like' data-type='p'><svg class='icon inline mr'><use xlink:href='#fa-thumbs-o-up'></use></svg>({$curPostObj['plikes']})</button> <button class='likeBtn' se-act='like' data-type='n'><svg class='icon inline mr'><use xlink:href='#fa-thumbs-o-down'></use></svg>({$curPostObj['pnolikes']})</button>" :
	"<span class='likeBtn'><i class='fa fa-thumbs-o-up '></i>({$curPostObj['plikes']})</span> <span class='likeBtn'><i class='fa fa-thumbs-o-down '></i>({$curPostObj['pnolikes']})</span>";

//</editor-fold>

//<editor-fold desc=" Header">

// Imagen
$imageWidth = '895';
$elHead = '';
switch ( $curPostObj['typeSub'] ) {
	//
	case 'video':
		$elHead = '<div class="mediaContainer">'.$curPostObj['contentExtra'].'</div>';
		break;
	//
	case 'gallery':
		//
		$sql_qry = "SELECT gal.gal_structure AS structure FROM sc_site_gallery AS gal WHERE gal.gal_id='{$curPostObj['contentExtraB']}' LIMIT 1;";
		$galleryData = $mysql->singleJSONGet($sql_qry);

		//
		$images = '';
		foreach ( $galleryData['d'] as $cImg ) {
			$images.= <<<HTML
<div class="mediaContainer">
<img src="/server/image/site/w_160/{$cImg['fName']}" title="{$cImg['title']}" data-fname="{$cImg['fName']}" data-content="{$cImg['description']}" />
</div>
HTML;

		}
		//
		$elHead = <<<HTML
<div class="mediaContainer">
<div class="se_gallery" se-plugin="se_gallery" data-ready="0" data-imagewidth="{$imageWidth}">{$images}</div>
</div>
HTML;

		break;
	//
	default:


		$elHead = ( empty($curPostObj['imgFileName']) ) ? "" : <<<html
<div class="mediaContainer">
<picture class="mainImg" itemprop="image">
	<source
		type="image/webp"
		srcset="/server/image/site/w_360/{$curPostObj['imgFileName']}.webp 360w,
				/server/image/site/w_720/{$curPostObj['imgFileName']}.webp 720w,
				/server/image/site/w_832/{$curPostObj['imgFileName']}.webp 832w"
		sizes= "(max-width:768px) 100vw,
				80vw"
	/> 
	<img class="mainImg"
		src="/server/image/site/w_{$imageWidth}/{$curPostObj['imgFileName']}.jpg"
		srcset="/server/image/site/w_360/{$curPostObj['imgFileName']}.jpg 360w,
				/server/image/site/w_720/{$curPostObj['imgFileName']}.jpg 720w,
				/server/image/site/w_832/{$curPostObj['imgFileName']}.jpg 832w"
		sizes= "(max-width:768px) 100vw,
				80vw"
		alt="{$curPostObj['imageAltText']}"
		width="{$curPostObj['imgWidth']}" height="{$curPostObj['imgHeigth']}"
	/>
</picture>
</div>\n
html;

		break;
}

//</editor-fold>

// ======================================
// Imprimir
// ======================================
//
$page['body'] = <<<HTML
<div class="fullTitle"><div class="wpContent">Blog</div></div>
<!-- INI:Content -->
<div class="wpContent grid full">
<div class="gr_sz09" itemscope itemtype="http://schema.org/Article">\n
<div id="blogPost" class="post" se-plugin="site_usr_blogPost" data-id="{$curPostObj['pId']}">
	<meta data-edelem="obj" data-type="hidden" data-name="id" data-value="{$curPostObj['pId']}" />
	<article>
		<h1 class="title" itemprop="name">{$curPostObj['pTitle']}</h1>
		<div class="info">
			<div><svg class="icon inline mr"><use xlink:href="#fa-clock-o" /></svg><span itemprop="datePublished" content="{$curPostObj['dtpub_iso']}">{$curPostObj['dtpub_simple']}</span></div>
			<div><a class="category" se-nav="se_middle" href="{$siteVars['server']['base_path']}/blog/category/{$curPostObj['curlname']}/"><svg class="icon inline mr"><use xlink:href="#fa-folder-o" /></svg>{$curPostObj['cname']}</a></div>
		</div>
		{$elHead}
		<div class="content">
<!-- INI: ArtBody -->
<div class="adv_text" itemprop="articleBody" data-edelem="obj" data-type="complex" data-name="content">
{$curPostObj['pcontent']}
</div>
<!-- END: ArtBody -->
		</div>
		<meta itemprop="interactionCount" content="UserComments:{$commentNumber}"/>
	</article>
	<div class="endContent">
		<div><div se-elem="postLike" class="aMenuL">{$likeString}</div></div>
		<div class="share"><button class="btn blue" se-plugin="shareAddon"><svg class="icon inline mr"><use xlink:href="#fa-share-alt" /></svg> Compartir</button></div>
	</div>
	{$userBottom}
</div>
{$commentsHTML}
</div>\n
<div  class="gr_sz03">\n
{$sideBarHTML}
</div>\n
</div>
<!-- END:Content -->\n\n
HTML;
//
