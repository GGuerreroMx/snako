<?php
// SITIO - Contacto
$page['head']['title'] = 'Contacto';
$page['head']['metaWord'] = 'Contacto';
$page['head']['metaDesc'].= 'Información y formulario de contacto.';
//
$page['head']['url'] = '/contacto/';

//
$referer = ( isset($_SERVER['HTTP_REFERER']) ) ? $_SERVER['HTTP_REFERER'] : '';

//
$textData = \snkeng\core\site\simpleText::load(['site_contact']);

//<editor-fold desc="Re Captcha">
//
$page['files']['ext'] = ['https://www.google.com/recaptcha/api.js?render=explicit&hl=es-419&onload=captchaLoad'];
\se_nav::pageFileGroupAdd(['google_captcha']);

//
$page['js'] .= <<<JS
function captchaLoad() {
	if ( typeof grecaptcha.render === 'undefined' ) { return; }
	grecaptcha.render('g_captcha', {'sitekey':'{$siteVars['extmod']['gCaptcha']['key']}'});
}\n
JS;
//
$page['mt'].= <<<JS
if ( window.grecaptcha ) { captchaLoad(); }\n
JS;
//
//</editor-fold>



// Contacto
$page['body'] = <<<HTML
<div class="fullTitle"><div class="wpContent">Contacto</div></div>
<div id="page_contact" class="wpContent grid">
	<div class="gr_sz04 gr_ps02">\n
		<form class="se_form" method="post" action="/ajax/api/site/contact/mail" se-plugin="simpleForm">
			<input type="hidden" name="ref" value="{$referer}" />
			<div class="separator required">
				<label>
					<div class="cont"><span class="title">Nombre</span><span class="desc"></span></div>
					<input type="text" name="uName" required pattern="[A-z\u00C0-\u00FF\ ]{3,50}" title="Nombre">
				</label>
			</div>
			<div class="separator required">
				<label>
					<div class="cont"><span class="title">E-Mail</span><span class="desc"></span></div>
					<input type="email" name="eMail" required title="Requerido para establecer contacto.">
				</label>
			</div>
			<div class="separator required">
				<label>
					<div class="cont"><span class="title">Mensaje</span><span class="desc"></span></div>
					<textarea name="uMsg" rows="7" required se-plugin="autoSize"></textarea>
				</label>
			</div>
			<div class="separator" id="g_captcha"></div>
			<button type="submit"><svg class="icon inline mr"><use xlink:href="#fa-mail-forward" /></svg>Enviar</button>
			<output se-elem="response"></output>
		</form>
	</div>
	<div class="gr_sz04">
		{$textData['elems']['site_contact']}
	</div>
</div>
HTML;
//