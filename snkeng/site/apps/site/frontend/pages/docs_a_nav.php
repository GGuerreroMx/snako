<?php
// Load defaults
\se_nav::pageFileGroupAdd(['site_usr_documentation']);

//
if ( se_login::check_loginLevel('admin') ) {
	// \se_nav::pageFileGroupAdd(['courses_adm']);
}

// Remover docs
\se_nav::next();

//
switch ( count(\se_nav::$path) ) {
	//
	case 0:
		se_killWithError('but how?');
		break;

	//
	case 1:
		// Mostrar documentación disponible
		require __DIR__ . '/docs_all.php';
		break;

	//
	case 2:
		$docTitle = preg_replace('/[^A-Za-z0-9\/\-_]/', '', \se_nav::next());

		//
		$sql_qry = <<<SQL
SELECT
docs.docs_id AS id,
docs_published AS isPub, docs_access_level AS accessLevel, docs_access_link_req AS isLinkReq
FROM sc_site_documents AS docs
WHERE docs.docs_title_url='{$docTitle}'
LIMIT 1;
SQL;
		$params['docData'] = $mysql->singleRowAssoc($sql_qry, [
			'int' => ['id', 'accessLevel'],
			'bool' => ['isPub', 'isLinkReq']
		]);

		//
		if ( empty($params['docData']) ) { \se_nav::invalidPage(); }

		// Mostrar índice documentación específica
		require __DIR__ . '/docs_main.php';
		break;

	//
	default:
		$cUrl = preg_replace('/[^A-Za-z0-9\/\-_]/', '', \se_nav::$pathStr);

		//
		$sql_qry = <<<SQL
SELECT
art_id AS id, cat_id AS catId,
art_title AS title, art_urltitle AS urlTitle, art_url AS fullUrl,
art_dt_mod AS dtMod,
art_order_hierarchy AS hierarchy,
art_meta_word AS metaWord, art_meta_desc AS metaDesc,
art_order_level AS orderLvl,
art_content AS content, art_content_extra AS contentExtra
FROM sc_site_articles
WHERE art_type='docs' AND art_url='{$cUrl}'
LIMIT 1;
SQL;
		$params['page'] = $mysql->singleRowAssoc($sql_qry, [
			'int' => ['id', 'catId', 'orderLvl']
		]);

		//
		if ( empty($params['page']) ) { \se_nav::invalidPage(); }

		//
		$sql_qry = <<<SQL
SELECT
docs.docs_id AS id,
docs.docs_title_normal AS title, docs.docs_title_url AS urlTitle,
docs_published AS isPub, docs_access_level AS accessLevel, docs_access_link_req AS isLinkReq
FROM sc_site_documents AS docs
WHERE docs.docs_id='{$params['page']['catId']}'
LIMIT 1;
SQL;
		$params['docData'] = $mysql->singleRowAssoc($sql_qry, [
			'int' => ['id', 'accessLevel'],
			'bool' => ['isPub', 'isLinkReq']
		]);

		//
		if ( empty($params['docData']) ) { \se_nav::invalidPage(); }

		// Analisis de permisos
		require __DIR__ . '/../func/permits.php';
		$params['docPermits'] = docs_get_permits_kill($params['docData']['id'], $params['docData']['isPub'], $params['docData']['accessLevel'], $params['docData']['isLinkReq']);

		// Update last page if is link req
		if ( $params['docData']['isLinkReq'] ) {
			$userId = se_login::getUserId();
			$upd_qry = "UPDATE sc_site_documents_access SET dacc_last_page={$params['page']['id']} WHERE dacc_id={$params['docPermits']['id']}";
			$mysql->submitQuery($upd_qry);
		}

		// Cache checking
		\se_nav::cacheCheckDate($params['page']['dtMod']);
		\se_nav::cacheFinalCheck();

		//
		if ( empty($params['page']) ) { \se_nav::invalidPage(); }

		//
		if ( isset($_GET['amp']) ) {
			require __DIR__ . '/docs_single_amp.php';
			exit();
		} else {
			require __DIR__ . '/docs_single.php';
		}
		break;
}
//
