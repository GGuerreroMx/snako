<?php
// Load defaults
\se_nav::pageFileGroupAdd(['site_usr_blog']);

//
$postLang = (require __DIR__ . '/../lang/' .$siteVars['site']['lang'].'.php');

// Links FIX FOR FALLBACK
if ( \se_nav::current() !== 'post' ) {
	\se_nav::next(); // Skip blog
}

//
switch ( \se_nav::next() ) {
	// autor
	case 'author':
		// Definir y revisar variables
		$autorName = preg_replace('/[^A-z0-9\-_]/', '', \se_nav::next());

		//
		if ( empty($autorName) ) { \se_nav::invalidPage(); }

		// Aprobar

		// Obtener el Id y hacer las operaciones
		$sql_qry = <<<SQL
SELECT
	a_id AS id,
	a_obj_name AS username, a_url_name_full AS userurl,
	a_img_crop AS image
FROM sb_objects_obj
WHERE a_url_name_full='{$autorName}'
LIMIT 1;
SQL;
		$authorData = $mysql->singleRowAssoc($sql_qry);

		//
		if ( empty($authorData) ) { \se_nav::invalidPage(); }

		// Read user settings
		$userData = snkeng\core\socnet\object_properties::readArray($authorData['id'], 'snkeng', 'profile');
		if ( empty($userData) ) { se_killWithError('User information not set'); }
		$userData = $userData['data'];

		//
		$params = [
			'type' => 'author',
			'data' => $authorData,
			'extraData' => $userData
		];

		require __DIR__ . '/blog_b_all.php';
		break;

	// categorías
	case 'category':
		// Definir y revisar variables
		$catName = preg_replace('/[^A-z0-9\-_]/', '', \se_nav::next());

		// Aprobar
		if ( empty($catName) ) { \se_nav::invalidPage(); }

		// Obtener el Id y hacer las operaciones
		$sql_qry = "SELECT cat_id
					FROM sc_site_category
					WHERE cat_urltitle='{$catName}'
					LIMIT 1;";
		$catId = $mysql->singleNumber($sql_qry);

		//
		if ( empty($catId) ) { \se_nav::invalidPage(); }

		$params = ['type' => 'category', 'catId' => $catId];
		require __DIR__ . '/blog_b_all.php';
		break;

	// Tags
	case 'tag':
		// Definir y revisar variables
		$tagName = preg_replace('/[^A-z0-9\-_]/', '', \se_nav::next());

		//
		if ( empty($tagName) ) { \se_nav::invalidPage(); }

		// Obtener el Id y hacer las operaciones
		$sql_qry = "SELECT tag_id
					FROM sc_site_tags
					WHERE tag_urltitle='{$tagName}'
					LIMIT 1;";
		$tagId = $mysql->singleNumber($sql_qry);

		//
		if ( empty($tagId) ) { \se_nav::invalidPage(); }

		//
		$params = ['type' => 'tags', 'tagId' => $tagId];
		require __DIR__ . '/blog_b_all.php';
		break;

	// videos
	case 'videos':
		$params = ['type' => 'videos'];
		require __DIR__.'/blog_b_all.php';
		break;

	// Buscar
	case 'search':
		$params = [
			'type' => 'search',
			's_words' => $_GET['sT']
		];
		require __DIR__ . '/blog_b_all.php';
		break;

	//
	case '':
	case null:
		\se_nav::cacheSetTime(1800);
		// Mostrar todos
		$params = ['type'=>''];
		require __DIR__ . '/blog_b_all.php';
		break;

	// Feed
	case 'feed':
		$rssType = ( \se_nav::next() === 'atom' ) ? 'atom' : 'rss2';
		snkeng\core\site\blogFeed::printFeed($rssType);
		break;

	// Error
	default:
		$urlTitle = \se_nav::current();

		//
		$pData = explode('-', $urlTitle);
		$pId = intval(end($pData));

		//
		if ( empty($pId) ) { \se_nav::invalidPage(); }

		//
		$sql_qry = <<<SQL
SELECT
	art_id AS pId, art_urltitle AS urlTitle, art_url AS fullUrl, art_type AS type, art_dt_mod AS dtMod
FROM sc_site_articles
WHERE art_id={$pId}
LIMIT 1;
SQL;

		$params['page'] = $mysql->singleRowAssoc(
			$sql_qry,
			[
				'int' => ['pId']
			]
		);

		//
		if ( empty($params['page']) || $params['page']['type'] !== 'blog' ) { \se_nav::invalidPage(); }

		\se_nav::cacheCheckDate($params['page']['dtMod']);


		//
		if ( $params['page']['fullUrl'] !== \se_nav::$pathStr ) {
			header('Location: ' . $siteVars['server']['url'] . $params['page']['fullUrl']);
			exit();
		}

		//
		if ( isset($_GET['amp']) && $_GET['amp'] ) {
			require __DIR__ . '/blog_b_single_amp.php';
			exit();
		} else {
			require __DIR__ . '/blog_b_single_normal.php';
		}
		break;
}
//
