<?php
// BLOG - Navigation (many)

//
$an_qry = (require __DIR__ . '/../nav/blog.php');

//
$typeXtra = '';
switch ( $params['type'] )
{
	// Autor
	case 'author':
		// Títulos
		$page['head']['title'] = "Posts by: " . $params['data']['username'];
		$page['head']['metaWord'] = "";
		$page['head']['metaDesc'].= $params['extraData']['desc'];
		$page['head']['url'] = $siteVars['site']['url']."/author/{$params['data']['userurl']}/";
		// Autor
		$an_qry['where']['author']['set'] = $params['data']['id'];

		//
		$userTw = ( true ) ? '' : '';

		//
		$typeXtra = <<<HTML
<div class="blog_user">
	<img class="userImg" src="/server/image/objects/w_160/{$params['data']['image']}" />
	<div class="uData">
		<div class="title">{$params['data']['username']}</div>
		<div class="desc">{$params['extraData']['desc']}</div>
		<div class="socnet">{$userTw}</div>
	</div>
</div>\n
HTML;
		break;

	// Categoría
	case 'category':
		// Extraer variables
		$sql_qry = "SELECT
						cat_title AS ctitle, cat_urltitle AS cutitle
					FROM sc_site_category
					WHERE cat_id={$params['catId']}
					LIMIT 1;";
		$curObj = $mysql->singleRowAssoc($sql_qry, [], [
			'errorKey' => 'cat',
			'errorDesc' => 'No fue posible leer la categoría'
		]);

		// Títulos
		$page['head']['title'] = "Categoría ".$curObj['ctitle'];
		$page['head']['metaWord'] = "";
		$page['head']['metaDesc'].= "";
		$page['head']['url'] = $siteVars['site']['url']."/blog/category/{$curObj['cutitle']}/";
		//
		$an_qry['where']['catId']['set'] = $params['catId'];
		//
		$typeXtra = <<<HTML
<h2>{$postLang['category']}: {$curObj['ctitle']}</h2>\n
HTML;
		break;

	// Búsqueda
	case 'search':
		// Títulos
		$page['head']['title'] = "Búsqueda ".$params['s_words'];
		$page['head']['metaWord'] = "";
		$page['head']['metaDesc'].= "";
		$page['head']['url'] = $siteVars['site']['url']."/buscar/";
		//
		$an_qry['where']['search']['set'] = $params['s_words'];
		//
		$typeXtra = <<<HTML
<h2>{$postLang['search']}: {$params['s_words']}</h2>\n
HTML;
		break;

	// Búsqueda
	case 'tags':
		//
		$sql_qry = <<<SQL
SELECT
cat_title AS ctitle, cat_urltitle AS cutitle
FROM sc_site_category
WHERE cat_id={$siteVars['catid']}
LIMIT 1;
SQL;
		$curObj = $mysql->singleRowAssoc($sql_qry, [], [
			'errorKey' => '',
			'errorDesc' => ''
		]);

		// Títulos
		$page['head']['title'] = "Tag ".$curObj['ctitle'];
		$page['head']['metaWord'] = "";
		$page['head']['metaDesc'].= "";
		$page['head']['url'] = $siteVars['site']['url']."/tags/";

		//
		$typeXtra = <<<HTML
<h2>{$postLang['tag']}: {$curObj['cutitle']}</h2>\n
HTML;
		break;

	// Sin modificaciones
	default:
		// Títulos
		$page['head']['title'] = "Blog";
		$page['head']['metaWord'] = "Blog";
		$page['head']['metaDesc'].= "Blog";
		$page['head']['url'] = $siteVars['site']['url']."/{$conditions['app']['site']['settings']['blog']['name']}/";
		break;
}

//
// SIDEBAR
//
$sideBarHTML = snkeng\core\site\sideBar::createSidebar([
	['name'=>'blogSearch', 'props'=>''],
	['name'=>'blogMosts', 'props'=>''],
	['name'=>'blogCategory', 'props'=>''],
	['name'=>'banner', 'props'=> [ 'size' => 1 ] ],
]);

//
// CONTENIDO
//

$post = new manual_ajaxjson();
$actions = '';

// ADMINISTRACIÓN
if ( se_login::check_loginLevel('sadmin') && false )
{
	// Mostrar administración
	$navData = [
		['icon' => 'plus', 'title' => 'Agregar', 'action' => 'add'],
	];
	$post->createNav($navData);

	// Categorías
	$catOpts = '';
	$sql_qry = "SELECT cat.cat_id, cat.cat_title FROM sc_site_category AS cat LIMIT 30;";
	if ( $mysql->execQuery($sql_qry) ) {
		$first = true;
		while ( $datos = $mysql->result->fetch_assoc() )
		{
			$catOpts .= ($first) ? '' : ', ';
			$catOpts .= "{$datos['cat_id']}:'{$datos['cat_title']}'";
			$first = false;
		}
	}

	// Formato JS
	$actions = <<<JS
{
	'add':
	{
		'ajax-action':'add',
		'action':'form',
		'form':{
			'title':'Nuevo Post',
			'action':'/api/blog/admin/post/add',
			'elems':[
			{'ftype':'text', 'vname':'ptitle', 'fname':'Nombre', 'fdesc':'', 'dtype':'str', 'dval':'', 'requiered':true},
			{'ftype':'list', 'vname':'catId', 'fname':'Categoría', 'fdesc':'', 'dval':'', 'requiered':true, 'opts':{{$catOpts}}}
			]
		}
	},
	'del':{'action':'del', 'action':'/api/blog/admin/post/del'}
}
JS;
	//
}

// Comentarios
/*
$comments = '';
switch ($siteVars['modset']['blog']['settings']['comments'])
{
	// Socnet
	case 1:
		$comments = '<div class="sep"><svg class="icon inline"><use xlink:href="#fa-comments" /></svg> <a se-nav="se_middle" href="{$siteVars['server']['base_path']}/blog/post/!pid;/!purltitle;/#comments">!msgcount; comentarios</a></div>';
		break;
	// Disqus
	case 2:
		if (!$siteVars['server']['test'])
		{
			$page['js'] .= "var disqus_shortname ='{$siteVars['extmod']['disqus']['shortname']}';\n";
			se.ajax.jsLoad('//' + $siteVars['extmod']['disqus']['shortname'] + '.disqus.com/count.js');
			$comments = '<div class="sep"><svg class="icon inline"><use xlink:href="#fa-comments" /></svg> <a se-nav="se_middle" href="{$siteVars['server']['base_path']}/blog/post/!pid;/!purltitle;/#disqus_thread" data-disqus-identifier="b_' . $params['pId'] . '">Comentarios</a></div>';
		} else {
			$comments = '<div class="sep"><svg class="icon inline"><use xlink:href="#fa-comments" /></svg> <a se-nav="se_middle" href="{$siteVars['server']['base_path']}/blog/post/!pid;/!purltitle;/#comments">Disqus no disponible en prueba</a></div>';
		}
		break;
	// Facebook <fb:comments-count href=http://example.com/></fb:comments-count>
	case 3:
		if (!$siteVars['server']['test']) {
			$comments = '<div class="sep"><svg class="icon inline"><use xlink:href="#fa-comments" /></svg> <a se-nav="se_middle" href="{$siteVars['server']['base_path']}/blog/post/!pid;/!purltitle;/#comments"><fb:comments-count href="'.$siteVars['server']['url'].'/blog/post/!pid;/!purltitle;/"></fb:comments-count> comentarios</a></div>';
		} else {
			$comments = '<div class="sep"><svg class="icon inline"><use xlink:href="#fa-comments" /></svg> <a se-nav="se_middle" href="{$siteVars['server']['base_path']}/blog/post/!pid;/!purltitle;/#comments">FB no disponible en prueba</a></div>';
		}

		break;
	default:
		break;
}
$author = '';
if ($siteVars['modset']['blog']['settings']['bloggerFocus'] === 1 )
{
	$author = '<div class="sep"><svg class="icon inline"><use xlink:href="#fa-user" /></svg> <a se-nav="se_middle" href="{$siteVars['server']['base_path']}/autor/!burlname;/" itemprop="author">!bname;</a></div>';
}
*/


// Estructura
$an_struct = <<<HTML
<article class="postMini status!published;" itemtype="http://schema.org/Article" data-ajsel="object" data-objid="!pid;" data-objtitle="!ptitle;">
<div class="body">
<div>
<a class="imgCont" se-nav="se_middle" href="!urlFull;">
	<picture itemprop="image">
		<source type="image/webp" srcset="/server/image/site/w_320/!image;.webp" /> 
		<img src="/server/image/site/w_320/!image;.jpg" />
	</picture>
</a>
</div>
<div class="contentSmall">
	<a class="category" se-nav="se_middle" href="/blog/category/!curlname;/">!ctitle;</a>
	<a class="title" se-nav="se_middle" href="!urlFull;" itemprop="name">!ptitle;</a>
	<div class="desc" itemprop="description">!mincontent;</div>
	<div class="info" title="!dtpub;"><svg class="icon inline mr"><use xlink:href="#fa-clock-o"></use></svg>!dtpub;</div>
</div>
</div>
</article>
HTML;
//
$an_empty = "No hay post que mostrar";
$exData = [
	'printStruct' => true,
	'js_url' => '/ajax/api/site/blog/postRead/',
	'actions' => $actions
];
// Preparar estructura
$post->createIniElem($an_qry, 'get', $an_struct, $an_empty, $exData);
// $post->debugQuery();

// Página
$page['body'] = <<<HTML
<div class="fullTitle"><div class="wpContent">{$postLang['type']}</div></div>
<!-- INI:BLOG -->
<div class="wpContent grid">
<!-- INI:POSTS -->
<div class="gr_sz09">
{$typeXtra}
{$post->r_html}
</div>
<!-- END:POSTS -->
<!-- INI:SIDEBAR -->
<div class="gr_sz03 hidden_tablet">\n
{$sideBarHTML}
</div>
<!-- END:SIDEBAR -->
</div>
<!-- END:BLOG -->\n
HTML;
//
