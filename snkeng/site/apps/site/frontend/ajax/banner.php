<?php

//
switch ( \se_nav::next() ) {
	//
	case 'link':
		// Verificar que existan resultados con las características y reiniciar en caso contrario
		$sql_select = <<<SQL
SELECT
	ban_id, ban_type, ban_size,
	ban_alt,
	ban_floc, ban_ftype, ban_url,
	ban_code
FROM sc_site_banners
WHERE ban_size={$size}
	AND ban_active=1
	AND ban_showed=0
	AND ( ban_curclicks<ban_maxclicks OR ban_maxclicks=0 )
	AND ( ban_curprints<ban_maxprints OR ban_maxprints=0 )
	AND ( now()<ban_dtend OR ban_dtend IS NULL )
	AND ( now()>ban_dtini OR ban_dtini IS NULL )
LIMIT 1;
SQL;
		$bannerData = $mysql->singleRowAssoc($sql_select);
		if ( empty($bannerData) ) {
			$sql_qry = "UPDATE sc_site_banners SET ban_showed=0 WHERE ban_active=1 AND ban_size={$size} AND ban_showed=1;";
			$mysql->submitQuery($sql_qry);
			// Retry
			$bannerData = $mysql->singleRowAssoc($sql_select);
		}

		// Print banner data
		if ( !empty($bannerData) ) {
			// Actualizar banner
			$sql_qry = "UPDATE sc_site_banners
						SET ban_curprints=ban_curprints+1, ban_showed=1
						WHERE ban_id={$bannerData['ban_id']};";
			$mysql->submitQuery($sql_qry);

			//
			switch ( $bannerData['ban_type'] ) {
				// File
				case '1':
					$returnData = "<a class='se_banner' se-plugin='banner' href='{$bannerData['ban_url']}' target='_blank' data-id='{$bannerData['ban_id']}'><img src='{$bannerData['ban_floc']}' alt='{$bannerData['ban_alt']}' /></a>\n";
					break;
				// Code
				case '2':
					$returnData = $bannerData['ban_code']."\n";
					break;
				// File Video (MP4 Only)
				case '3':
					$returnData = <<<HTML
<a class='se_banner' se-plugin='banner' href='{$bannerData['ban_url']}' target='_blank' data-id='{$bannerData['ban_id']}'><video autoplay><source type="video/mp4" src="{$bannerData['ban_floc']}" />Your browser does not support the video tag.</video></a>
HTML;
					//
					break;
				//
				default:
					se_killWithError("Banner Type unrecognized.", $bannerData['type']);
					break;
			}
		} else {
			$returnData = "<!-- BANNER ({$size}): Not Available -->\n";
		}

		//
		$repsonse['d'] = $returnData;
		break;

	//
	default:
		se_killWithError('URL no válido.', $pathNext);
		break;
}
//
