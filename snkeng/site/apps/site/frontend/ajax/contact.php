<?php
//
switch ( \se_nav::next() ) {
	//
	case 'mail':
		// Revisar anti bot
		seLoad_engFunc('googleCaptcha');
		if ( !recaptcha_vertify() ) {
			se_killWithError("Captcha no aprobado.");
		}

		// Revisar operación per se
		$rData = \se_saveData::postParsing(
			$response,
			[
				'uName' => ['name' => 'Nombre', 'type' => 'str', 'lMin' => 0, 'lMax' => 50, 'filter' => 'simpleTitle', 'process' => false, 'validate' => true],
				'eMail' => ['name' => 'Email', 'type' => 'str', 'lMin' => 0, 'lMax' => 75, 'filter' => 'email', 'process' => false, 'validate' => true],
				'uTel' => ['name' => 'Teléfono', 'type' => 'str', 'lMin' => 0, 'lMax' => 20, 'filter' => '', 'process' => false, 'validate' => false],
				'ref' => ['name' => 'URL Anterior', 'type' => 'str', 'lMin' => 0, 'lMax' => 150, 'filter' => 'url', 'process' => false, 'validate' => false],
				'uMsg' => ['name' => 'Mensaje', 'type' => 'str', 'lMin' => 0, 'lMax' => 600, 'filter' => 'simpleText', 'process' => false, 'validate' => false]
			]
		);
		// Datos
		$to = "Contacto <{$siteVars['admin']['contact']}>"; // Caso especial
		$title = 'Contacto - '.$siteVars['site']['name'];
		$rData['ref'] = urldecode($rData['ref']);
		$message = <<<TEXT
NOMBRE: {$rData['uName']}
E-MAIL: {$rData['eMail']}
TEL: {$rData['uTel']}
URL Anterior: {$rData['ref']}
MENSAJE:
----
{$rData['uMsg']}
----
TEXT;

		// Operaciones
		seLoad_engFunc('mailSend');
		$mail = se_sendHTMLMail('', $to, [
			'title' => $title,
			'message' => $message,
			'replyTo' => $rData['eMail']
		]); // / st_content / res / mailing / mail_01_simple.html

		// Exitoso
		$response['s']['d'] = 'Información enviada';
		$response['s']['ex'] = 'Pronto nos pondremos en contacto con usted.';
		$response['debug']['mail'] = $mail['debug']['mail'];
		break;
	//
	default:
		\se_nav::invalidPage();
		break;
}
//
