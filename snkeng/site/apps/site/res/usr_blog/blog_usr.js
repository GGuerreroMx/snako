'use strict';
// Blog post interaction
se.plugin.site_usr_blogPost = function(plugElem, plugOptions) {
	// Binds
	let pDefaults = {
			callback:null
		},
		pSettings = se.object.merge(pDefaults, plugOptions),
		params = {
			pId:plugElem.se_data('id')
		},
		postLikeObj = plugElem.querySelector('div[se-elem="postLike"]');


	//
	function init() {
		// Bindings
		plugElem.se_on('click', 'button[se-act]', btnActions);
	}

	//
	function btnActions(ev, cBtn) {
		ev.preventDefault();
		switch ( cBtn.se_attr('se-act') ) {
			//
			case 'like':
				postLike(cBtn.se_data('type'));
				break;
			//
			default:
				console.error("Caso del botón no programado.", cBtn.se_attr('se-act'), cBtn);
				break;
		}
	}

	//
	function postLike(type) {
		se.ajax.json('/ajax/api/site/blog/postLike/', {'pId':params.pId, 'vote':type}, {
			onSuccess:(msg) => {
				postLikeObj.se_html(msg['d']);
			},
			onFail:(msg) => {
				console.log(msg['s']['e']);
			}
		});
	}

	// Expose
	init();
	return {};
};
//
