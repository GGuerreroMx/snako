<?php
//
switch ( \se_nav::next() ) {
	//
	case 'versions':
		require __DIR__ . '/pages/versions.php';
		break;

	// Página principal
	case '':
	case null:
		require __DIR__ . '/pages/a_index.php';
		break;

	// Otros
	default:
		\se_nav::invalidPage();
		break;
}
//
