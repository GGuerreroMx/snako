<?php
//
\se_nav::cacheCheckFile(__FILE__);
\se_nav::cacheFinalCheck();

//
$params['page']['ajax'].= '/versions';

// Query
$an_qry = (require __DIR__ . '/../navs/versions.php');

//
$an_empty = "No hay elementos que mostrar.";
// Estructura
$an_struct = <<<HTML
<tr se-ajaxelem="obj" se-ajaxact="upd" data-objid="!id;" data-objtitle="!title;">
	<td>!id;</td>
	<td>!titleVersion;</td>
	<td>!titleName;</td>
	<td>!dtAdd;</td>
	<td>!dtMod;</td>
	<td>
		<button class="btn small" se-ajaxbtn="editVersioning" title="Datos de la versión"><svg class="icon inline"><use xlink:href="#fa-gear" /></svg></button>
		<button class="btn small" se-ajaxbtn="editComment" title="Comentario"><svg class="icon inline"><use xlink:href="#fa-gear" /></svg></button>
		<button class="btn small" se-ajaxbtn="editDBStruct" title="DB Estructura"><svg class="icon inline"><use xlink:href="#fa-gear" /></svg></button>
		<button class="btn small" se-ajaxbtn="editDBBase" title="DB Inicial"><svg class="icon inline"><use xlink:href="#fa-gear" /></svg></button>
		<button class="btn small" se-ajaxbtn="del" title="Borrar"><svg class="icon inline"><use xlink:href="#fa-trash-o" /></svg></button>
	</td>
</tr>
HTML;
//
$exData = [
	'js_url' => $params['page']['ajax'].'/readAll',
	'js_defaults' => "",
	'printElements' => false,
	'printType' => 'table',
	'tableWide' => true,
	'tableHead' => [
		['name' => 'ID', 'filter' => 1, 'fName' => 'id'],
		['name' => 'Version', 'filter' => 1, 'fName' => 'titleVersion'],
		['name' => 'Nombre', 'filter' => 1, 'fName' => 'titleName'],
		['name' => 'dtAdd'],
		['name' => 'dtMod'],
		['name' => 'Acciones'],
	],
	'settings' => ['nav' => true],
	'actions' => <<<JS
{
	'add':
	{
		'ajax-action':'add',
		'ajax-elem':'none',
		'print':false,
		'menu':{'title':'Agregar', 'icon':'fa-plus'},
		'action':'form',
		'form':{
			'title':'Nueva versión',
			'save_url':'{$params['page']['ajax']}/add',
			'actName':'Agregar',
			'elems':[
				{'ftype':'text', 'vname':'titleVersion', 'fname':'Title Version', 'fdesc':'', 'dtype':'str', 'requiered':true},
				{'ftype':'text', 'vname':'titleName', 'fname':'Title Name', 'fdesc':'', 'dtype':'str', 'requiered':true},
				{'ftype':'textarea', 'vname':'comments', 'fname':'Comments', 'fdesc':'', 'dtype':'str', 'requiered':true},
			]
		}
	},
	'editVersioning':
	{
		'ajax-action':'edit',
		'ajax-elem':'none',
		'action':'form',
		'form':{
			'title':'Editar Version',
			'load_url':'{$params['page']['ajax']}/editVersionRead',
			'save_url':'{$params['page']['ajax']}/editVersionUpd',
			'actName':'Editar',
			'elems':[
				{'ftype':'hidden', 'vname':'id', 'fname':'ID', 'dtype':'int', 'requiered':true},
				{'ftype':'text', 'vname':'titleVersion', 'fname':'Title Version', 'fdesc':'', 'dtype':'str', 'requiered':true},
				{'ftype':'text', 'vname':'titleName', 'fname':'Title Name', 'fdesc':'', 'dtype':'str', 'requiered':true},
			]
		}
	},
	'editComment':
	{
		'ajax-action':'edit',
		'ajax-elem':'none',
		'action':'form',
		'form':{
			'title':'Editar Comentario',
			'load_url':'{$params['page']['ajax']}/editCommentRead',
			'save_url':'{$params['page']['ajax']}/editCommentUpd',
			'actName':'Editar',
			'elems':[
				{'ftype':'hidden', 'vname':'id', 'fname':'ID', 'dtype':'int', 'requiered':true},
				{'ftype':'textarea', 'vname':'comments', 'fname':'Comments', 'fdesc':'', 'lMax':65000, 'dtype':'str', 'requiered':true},
			]
		}
	},
	'editDBStruct':
	{
		'ajax-action':'edit',
		'ajax-elem':'none',
		'action':'form',
		'form':{
			'title':'Editar DB Estructura',
			'load_url':'{$params['page']['ajax']}/editDBStructRead',
			'save_url':'{$params['page']['ajax']}/editDBStructUpd',
			'actName':'Editar',
			'elems':[
				{'ftype':'hidden', 'vname':'id', 'fname':'ID', 'dtype':'int', 'requiered':true},
				{'ftype':'textarea', 'vname':'dbStructure', 'fname':'DB Structure', 'fdesc':'', 'lMax':65000, 'dtype':'str', 'requiered':true},
			]
		}
	},
	'editDBBase':
	{
		'ajax-action':'edit',
		'ajax-elem':'none',
		'action':'form',
		'form':{
			'title':'Editar',
			'load_url':'{$params['page']['ajax']}/edidDBBaseRead',
			'save_url':'{$params['page']['ajax']}/edidDBBaseUpd',
			'actName':'Editar',
			'elems':[
				{'ftype':'hidden', 'vname':'id', 'fname':'ID', 'dtype':'int', 'requiered':true},
				{'ftype':'textarea', 'vname':'dbPure', 'fname':'DB Pure', 'fdesc':'', 'lMax':65000, 'dtype':'str', 'requiered':true},
			]
		}
	},
	'del':{'action':'del', 'ajax-elem':'obj', 'del_url':'{$params['page']['ajax']}/del'}
}
JS
];
//

//
$post = new manual_ajaxjson();
$post->createIniElem($an_qry['versions'], 'get', $an_struct, $an_empty, $exData);

// $post->debugQuery();

$page['head']['title'] = 'Versiones';

// Page
$page['body'] = <<<HTML
<div class="pageSubTitle">Versiones</div>

{$post->r_html}
HTML;
//
