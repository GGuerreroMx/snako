<?php

//
switch ( \se_nav::next() )
{
	// Agregar
	case 'add':
		//
		\se_saveData::sql_complex_rowAdd($response,
			[
				'titleVersion' => ['name' => 'Nombre Versión',   'type' => 'str', 'lMin' => 0, 'lMax' => 50, 'filter' => 'simpleTitle', 'process' => false, 'validate' => true],
				'titleName' => ['name' => 'Nombre Título',   'type' => 'str', 'lMin' => 0, 'lMax' => 50, 'filter' => 'urlTitle', 'process' => false, 'validate' => true],
				'comments' => ['name'=>'Comments', 'type'=>'str', 'process'=>false, 'validate'=>true],
			],
			[
				'elems' => [
					'sev_title_version' => 'titleVersion',
					'sev_title_name' => 'titleName',
					'sev_comments' => 'comments',
				],
				'tName' => 'st_se_versions',
				'tId' => ['nm' => 'id', 'db' => 'sev_id']
			]
		);
		break;


	//
	case 'editVersionRead':
		\se_saveData::sql_complex_rowRead(
			$response,
			[
				'elems' => [
					'sev_title_version' => 'titleVersion',
					'sev_title_name' => 'titleName',
				],
				'tName' => 'st_se_versions',
				'tId' => ['nm' => 'id', 'db' => 'sev_id']
			],
			'id'
		);
		break;

	//
	case 'editVersionUpd':
		// Validar
		\se_saveData::sql_complex_rowUpd($response,
			[
				'id' => ['name'=>'ID', 'type'=>'int', 'process'=>false, 'validate'=>true],
				'titleVersion' => ['name'=>'Title Version', 'type'=>'str', 'process'=>false, 'validate'=>true],
				'titleName' => ['name'=>'Title Name', 'type'=>'str', 'process'=>false, 'validate'=>true],
			],
			[
				'elems' => [
					'sev_title_version' => 'titleVersion',
					'sev_title_name' => 'titleName',
				],
				'tName' => 'st_se_versions',
				'tId' => ['nm' => 'id', 'db' => 'sev_id']
			]
		);
		break;

	//
	case 'editCommentRead':
		\se_saveData::sql_complex_rowRead(
			$response,
			[
				'elems' => [
					'sev_comments' => 'comments',
				],
				'tName' => 'st_se_versions',
				'tId' => ['nm' => 'id', 'db' => 'sev_id']
			],
			'id'
		);
		break;

	//
	case 'editCommentUpd':
		// Validar
		\se_saveData::sql_complex_rowUpd($response,
			[
				'id' => ['name'=>'ID', 'type'=>'int', 'process'=>false, 'validate'=>true],
				'comments' => ['name'=>'Comments', 'type'=>'str', 'process'=>false, 'validate'=>true],
			],
			[
				'elems' => [
					'sev_comments' => 'comments',
				],
				'tName' => 'st_se_versions',
				'tId' => ['nm' => 'id', 'db' => 'sev_id']
			]
		);
		break;

	//
	case 'editDBStructRead':
		\se_saveData::sql_complex_rowRead(
			$response,
			[
				'elems' => [
					'sev_db_structure' => 'dbStructure',
				],
				'tName' => 'st_se_versions',
				'tId' => ['nm' => 'id', 'db' => 'sev_id']
			],
			'id'
		);
		break;

	//
	case 'editDBStructUpd':
		// Validar
		\se_saveData::sql_complex_rowUpd($response,
			[
				'id' => ['name'=>'ID', 'type'=>'int', 'process'=>false, 'validate'=>true],
				'dbStructure' => ['name'=>'DB Structure', 'type'=>'str', 'process'=>false, 'validate'=>true],
			],
			[
				'elems' => [
					'sev_db_structure' => 'dbStructure',
				],
				'tName' => 'st_se_versions',
				'tId' => ['nm' => 'id', 'db' => 'sev_id']
			]
		);
		break;

	//
	case 'edidDBBaseRead':
		\se_saveData::sql_complex_rowRead(
			$response,
			[
				'elems' => [
					'sev_db_pure' => 'dbPure',
				],
				'tName' => 'st_se_versions',
				'tId' => ['nm' => 'id', 'db' => 'sev_id']
			],
			'id'
		);
		break;

	//
	case 'edidDBBaseUpd':
		// Validar
		\se_saveData::sql_complex_rowUpd($response,
			[
				'id' => ['name'=>'ID', 'type'=>'int', 'process'=>false, 'validate'=>true],
				'dbPure' => ['name'=>'DB Pure', 'type'=>'str', 'process'=>false, 'validate'=>true],
			],
			[
				'elems' => [
					'sev_db_pure' => 'dbPure',
				],
				'tName' => 'st_se_versions',
				'tId' => ['nm' => 'id', 'db' => 'sev_id']
			]
		);
		break;

	// Borrar
	case 'del':
		\se_saveData::sql_table_delRow($response,
			[
				'tName' => 'sc_site_category',
				'tId' => ['db' => 'cat_id']
			],
			'id'
		);
		break;

	// Leer todas
	case 'readAll':
		$an_qry = (require(__DIR__.'/../navs/versions.php'));

		// Preparar estructura
		$post = new manual_ajaxjson();
		$post->createJSON($an_qry['versions'], 'post');
		break;

	//
	default:
		\se_nav::invalidPage();
		break;
}
//
