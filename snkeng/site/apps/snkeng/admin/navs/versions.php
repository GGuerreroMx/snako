<?php
//
$nav = [
	'versions' => [
		'sel' => "sev.sev_id AS id,
					sev.sev_dt_add AS dtAdd, sev.sev_dt_mod AS dtMod,
					sev.sev_title_version AS titleVersion, sev.sev_title_name AS titleName,
					sev.sev_comments AS comments,
					sev.sev_db_structure AS dbStructure, sev.sev_db_pure AS dbPure",
		'from' => 'st_se_versions AS sev',
		'lim' => 20,
		'where' => [
			['name' => 'ID', 'db' => 'sev.sev_id', 'var' => 'id', 'vtype' => 'int', 'stype' => 'eq'],
			['name' => 'Title Version', 'db' => 'sev.sev_title_version', 'var' => 'title', 'vtype' => 'str', 'stype' => 'like'],
			['name' => 'Title Name', 'db' => 'sev.sev_title_name', 'var' => 'user', 'vtype' => 'str', 'stype' => 'like']
		],
		'order' => [
			['Name' => 'ID', 'db' => 'sev.sev_id', 'set' => 'DESC']
		]
	]
];

//
return $nav;
