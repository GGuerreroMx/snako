<?php
// Validation
if ( !isset($_SERVER['HTTP_AUTHORIZATION']) ) { die("clave no definida."); }
$authString = substr($_SERVER['HTTP_AUTHORIZATION'], 6);
if ( empty($authString) ) { die("no hay clave auth"); }
$authString = base64_decode($authString);
list($authUser, $authPass) = explode(':', $authString, 2);
if ( empty($authUser) || empty($authPass) ) { die("Claves de autorización incompletas. U:{$authUser}. P:{$authPass}."); }
if ( $authUser !== 'abc' || $authPass !== 'def' ) { die("Claves de autorización no válidas. U:{$authUser}. P:{$authPass}."); }



//
switch ( \se_nav::next() )
{
	//
	case 'core':
		$fileLocation = $_SERVER['DOCUMENT_ROOT'] . '/res/files/latest_version.zip';

		//
		switch ( \se_nav::next() )
		{
			//
			case 'upload':
				if ( empty($_FILES['file']) ) { die("No fue subido el archivo."); }

				//
				$fileType = strtolower(pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION));
				if ( $fileType !== 'zip' ) {
					die("formato de archivo no válido.");
				}

				// Mover el archivo
				if ( !move_uploaded_file($_FILES['file']["tmp_name"], $fileLocation) ) {
					se_killWithError('Unable to copy file (for main)');
				}

				// Update update information
				$sql_qry = <<<SQL
SELECT sev_id AS id, sev_title_version AS version
FROM st_se_versions
ORDER BY sev_id DESC
LIMIT 1;
SQL;
				$versionData = $mysql->singleRowAssoc($sql_qry,
					[
						'int' => ['id']
					],
					[
						'errorKey' => '',
						'errorDesc' => '',
					]
				);

				//
				$verData = explode('.', $versionData['version']);

				//
				$verData[3] = intval($verData[3]);
				$verData[3]++;

				$newVersion = implode('.', $verData);

				// Update
				$sql_upd = <<<SQL
UPDATE st_se_versions SET sev_title_version='{$newVersion}' WHERE sev_id={$versionData['id']};
SQL;
				$mysql->submitQuery($sql_upd);

				echo "ok: file uploaded";
				exit();
				break;

			//
			case 'download':
				// Detect file existance
				if ( !file_exists($fileLocation) ) {
					die("archivo no encontrado...?");
				}

				// Make it downloadable
				header('Content-Description: File Transfer');
				header('Content-Type: application/octet-stream');
				header('Content-Disposition: attachment; filename="'.basename($fileLocation).'"');
				header('Expires: 0');
				header('Cache-Control: must-revalidate');
				header('Pragma: public');
				header('Content-Length: ' . filesize($fileLocation));
				readfile($fileLocation);
				exit;
				break;

			//
			default:
				\se_nav::invalidPage();
				break;
		}
		//
		break;

	//
	default:
		\se_nav::invalidPage();
		break;
}
//
