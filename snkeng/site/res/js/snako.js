// Plugin menu
se.plugin.navMenu = function (element, options) {
	var defaults = { callback:null },
		plugin = this;
	//
	plugin.settings = {};
	// Funciones
	var init = function (e, cEl) {
			// Inits
			plugin.settings = se.object.merge(defaults, options);
			// Binds
			element.se_on('click', '.navigation .background', closeMenus);
			element.se_on('click', 'a[se-m-nav]', navigate);
			element.se_on('click', 'button[data-menu]', menuOp);
			element.se_on('transitionend', 'div[data-menu]', checkOp);
		},
		navigate = function (e, cEl) {
			e.preventDefault();
			se.ajax.pageLink(cEl.href, cEl.getAttribute('se-m-nav'));
			closeMenus();
		},
		closeMenus = function () {
			element.querySelector('.navigation > .sel').se_classDel('sel');
			// Firefox bug
			checkOp();
		},
		menuOp = function (e, cEl) {
			e.stopPropagation();
			var target = element.querySelector('.navigation [data-menu="' + cEl.se_data('menu') + '"]');
			if ( target ) {
				if ( target.se_classHas('sel') ) {
					//
					target.se_classDel('sel');
				} else {
					element.se_classAdd('full');
					// Cerrar el resto
					element.querySelectorAll('.navigation > .sel').se_classDel('sel');
					//
					target.se_classAdd('sel');
				}
			} else {
				console.log("menu error - no definido target.", cEl.se_data('target'));
			}
		},
		checkOp = function (e, cObj) {
			var openMenus = element.querySelectorAll('.navigation > .sel');
			if ( openMenus.length === 0 ) {
				console.log("cerrar");
				element.se_classDel('full');
			}
		};
	//
	init();
};
