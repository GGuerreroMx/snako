<?php
//
return [
    'server' => [
	    'test' => false,
	    'local' => false,
	    'base_path' => '',
	    'url' => ''
    ],
	'site' => [
		'url'=>'http://www.snako.dev',
		'surl'=>'https://www.snako.dev',
		'img'=>'/st_res/img/st_logo.jpg',
		'name'=>'Snako',
		'desc'=>'A Web Developing Framework designed for asyncronic and web optimized aplications.',
		'domain'=>'snako.dev',
		'baseurl'=>'www.snako.dev',
		//
		'langs' => ['en_US'],
		'lang' => 'en_US'
	],
	'page' => [
		'complete' => false,
		'template' => 'main',
		'base' => '' // Empty -> defaults to system
	],
	'extmod' => [
		'gA' =>'UA-38219329-1',
		'gCaptcha' => ['key' => '6Ld4PAYTAAAAAO3zu8XZ1XTcsl-fK439Py4QpaPz', 'secret' => '6Ld4PAYTAAAAAKs32IR0w1xfoI1um7s_IkxSt8EM'],
		'disqus'=>['set'=>true, 'shortname'=> 'snako']
	],
	'snk_eng' => [
		'ieCode'=>'aeYSwCnYte2kJE23y3B2Vtk'
	],
	'admin' => [
		'mail'=>'adenoidsnake@hotmail.com',
		'contact'=>'gguerreromx@hotmail.com'
	],
	'base_path' => ''
];