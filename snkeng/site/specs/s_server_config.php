<?php
// Configuración
return [
	'locations' => [
		'snako.cora.snako.dev' => [
			'db' => 'local_test',
			'lang' => 'en_US',
			'local' => true,
			'test' => true,
		],
		'www.snako.dev' => [
			'db' => 'live',
			'lang' => 'en_US',
			'local' => false,
			'test' => false,
			'secure' => true,
		],
		'snako.dev' => [
			'redirect' => 'https://www.snako.dev'
		],
	],
	'db'=> [
		'live'=>[
			'db'=>'db_snako',
			'adm' => ['snk_adm', 'cXUrjkWW5EguJWEEzpEQTyxV83YVJe9StSXvTnwdFrLDvxYr'],
			'usr' => ['snk_usr', 'hps5vscevues94nsRnmz3dG8nJZwFDguCC8c3TF9MQ9M8Ffp'],
		],
		'local_test'=>[
			'db'=>'db_snako',
			'adm' => ['root', 'snkTst'],
			'usr' => ['root', 'snkTst'],
		]
	],
];
